<?php

namespace DPDFrance\ShippingM2\Setup;

use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\InstallSchemaInterface;

class InstallSchema implements InstallSchemaInterface {
    
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        $installer = $setup;
        $installer->startSetup();

        $connection = $setup->getConnection();
        
        if (!$installer->tableExists('dpd_option')) {
            $table = $installer->getConnection()->newTable($installer->getTable('dpd_option'))
                    ->addColumn('entity_id', Table::TYPE_INTEGER, null, [
                        'identity' => true,
                        'nullable' => false,
                        'primary'  => true,
                        'unsigned' => true
                    ], 'Entity ID')
                    ->addColumn('order_id', Table::TYPE_TEXT, 255, [
                        'nullable' => false                        
                    ], 'Order ID')
                    ->addColumn('advalorem', Table::TYPE_BOOLEAN, null, [
                        'nullable' => false          
                    ], 'Ad Valorem')
                    ->addColumn('retour', Table::TYPE_INTEGER, null, [
                        'nullable' => false
                    ], 'Retour');
            
            $installer->getConnection()->createTable($table);
        }
        
        $installer->endSetup();
    }

}