<?php

namespace DPDFrance\ShippingM2\Setup;

use \Magento\Framework\Setup\UpgradeSchemaInterface;
use \Magento\Framework\Setup\ModuleContextInterface;
use \Magento\Framework\Setup\SchemaSetupInterface;
use \Magento\Framework\DB\Ddl\Table;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        $setup->startSetup();

        // when version < 0.9.1
        if (version_compare($context->getVersion(), '0.9.2', '<')) {

            // Get module table
            $table = $setup->getTable('dpd_option');
            $connection = $setup->getConnection();

            // Check if the table already exists
            if ($connection->isTableExists($table) == true) {
                
                $connection->modifyColumn(
                    "dpd_option",
                    'order_id',
                    [
                        'type' => Table::TYPE_TEXT,
                        'length' => '255',
                    ]
                );
            }
        }

        $setup->endSetup();
    }
}
