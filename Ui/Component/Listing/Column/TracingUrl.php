<?php

namespace DPDFrance\ShippingM2\Ui\Component\Listing\Column;

use Magento\Sales\Model\OrderFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;

//Gestion de l'url tracing dans la gestion des expéditions
class TracingUrl extends \Magento\Ui\Component\Listing\Columns\Column {
    /**
     *
     * @var Magento\Sales\Model\OrderFactory
     */
    protected $_orderFactory;
    
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        OrderFactory $orderFactory,
        array $components = [],
        array $data = []) {
        
        $this->_orderFactory = $orderFactory;
        
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }
    
    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {          
        foreach ($dataSource["data"]["items"] as & $item) {            
            $customer_id = "";
            $customer_ag = "";
			
            switch ($item["shipping_method"]) {
                case "dpdclassic_dpdclassic":
					$customer_ag = \DPDFrance\ShippingM2\Helper\Adminhtml\Carrier\DPDClassicHelper::getInstance()->getAgenceDPD();
                    $customer_id = \DPDFrance\ShippingM2\Helper\Adminhtml\Carrier\DPDClassicHelper::getInstance()->getContract();
                    break;
                case "dpdpredict_dpdpredict":
					$customer_ag = \DPDFrance\ShippingM2\Helper\Adminhtml\Carrier\DPDPredictHelper::getInstance()->getAgenceDPD();
                    $customer_id = \DPDFrance\ShippingM2\Helper\Adminhtml\Carrier\DPDPredictHelper::getInstance()->getContract();
                    break;
                case "dpdrelais_dpdrelais":
					$customer_ag = \DPDFrance\ShippingM2\Helper\Adminhtml\Carrier\DPDRelaisHelper::getInstance()->getAgenceDPD();
                    $customer_id = \DPDFrance\ShippingM2\Helper\Adminhtml\Carrier\DPDRelaisHelper::getInstance()->getContract();
                    break;
            }
            
			$url = "https://www.dpd.fr/tracex_" . $item["increment_id"]. "_" . $customer_ag . $customer_id;
            $a = "<a href=" . $url . " target='_blank'>lien</a>";     
            
            $item[$this->getData('name')] = $a;            
        }
        
        return $dataSource;
    }
}