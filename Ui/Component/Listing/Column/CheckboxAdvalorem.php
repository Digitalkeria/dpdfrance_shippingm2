<?php

namespace DPDFrance\ShippingM2\Ui\Component\Listing\Column;

use DPDFrance\ShippingM2\Helper\Adminhtml\SetupHelper;
use Magento\Sales\Model\OrderFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

//Gestion du checkbox de l'assurance dans la gestion des expéditions
class CheckboxAdvalorem extends Column
{
    /**
     * @var Magento\Sales\Model\OrderFactory
     */
    protected $_orderFactory;

    public function __construct(
        ContextInterface   $context,
        UiComponentFactory $uiComponentFactory,
        OrderFactory       $orderFactory,
        array              $components = [],
        array              $data = [])
    {
        $this->_orderFactory = $orderFactory;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        foreach ($dataSource["data"]["items"] as & $item) {
            $advaloremActivated           = SetupHelper::getInstance()->getAssurance();
            $advalorem                    = $advaloremActivated === '0' ? "" : "checked";
            $html                         = "<input type='checkbox' " . $advalorem . " id='advalorem_" . $item["entity_id"] . "' onchange='advalorem(\"" . $item["entity_id"] . "\")'/>";
            $item[$this->getData('name')] = $html;
        }

        return $dataSource;
    }
}
