<?php

namespace DPDFrance\ShippingM2\Ui\Component\Listing\Column;

use Magento\Sales\Model\OrderFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;

//Gestion du checkbox de l'option retour dans la gestion des expéditions
class CheckboxRetour extends \Magento\Ui\Component\Listing\Columns\Column {
    /**
     *
     * @var Magento\Sales\Model\OrderFactory
     */
    protected $_orderFactory;
    
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        OrderFactory $orderFactory,
        array $components = [],
        array $data = []) {
        
        $this->_orderFactory = $orderFactory;
        
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }
    
    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        foreach ($dataSource["data"]["items"] as & $item) {            

            $retourActivated = \DPDFrance\ShippingM2\Helper\Adminhtml\SetupHelper::getInstance()->getRetour();
            
            if ($retourActivated == 0) {
                $retourActivated = "disabled";
                $retour = "";
            } else {
                $retourActivated = "";
				$retour = "checked";
            }
            
            $html = "<input " . $retourActivated . " type='checkbox' " . $retour . " id='retour_" . $item["entity_id"] . "' onchange='retour(\"" . $item["entity_id"] . "\")'/>";   
                        
            $item[$this->getData('name')] = $html;                        
        }                
        
        return $dataSource;
    }
}