<?php

namespace DPDFrance\ShippingM2\Block\Adminhtml\Shipments;

use DPDFrance\ShippingM2\Helper\Adminhtml\Carrier\DPDClassicHelper;
use DPDFrance\ShippingM2\Helper\Adminhtml\Carrier\DPDPredictHelper;
use DPDFrance\ShippingM2\Helper\Adminhtml\Carrier\DPDRelaisHelper;
use DPDFrance\ShippingM2\Helper\Adminhtml\SetupHelper;
use Magento\Framework\Phrase;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

/**
 * * Ce block permet de transmettre des variables aux templates, ici le template shipments/index.phtml
 */
class Shipments extends Template
{
    private $setup_helper;
    private $dpdclassic_helper;
    private $dpdpredict_helper;
    private $dpdrelais_helper;

    /**
     * @param Context $context
     */
    public function __construct(Context $context)
    {
        parent::__construct($context);

        $this->setup_helper      = SetupHelper::getInstance();
        $this->dpdclassic_helper = DPDClassicHelper::getInstance();
        $this->dpdpredict_helper = DPDPredictHelper::getInstance();
        $this->dpdrelais_helper  = DPDRelaisHelper::getInstance();
    }

    /**
     * @return SetupHelper
     */
    public function getSetupHelper(): SetupHelper
    {
        return $this->setup_helper;
    }

    /**
     * @return DPDClassicHelper
     */
    public function getDPDClassicHelper(): DPDClassicHelper
    {
        return $this->dpdclassic_helper;
    }

    /**
     * @return DPDPredictHelper
     */
    public function getDPDPredictHelper(): DPDPredictHelper
    {
        return $this->dpdpredict_helper;
    }

    /**
     * @return DPDRelaisHelper
     */
    public function getDPDRelaisHelper(): DPDRelaisHelper
    {
        return $this->dpdrelais_helper;
    }

    /**
     * Return header text for form
     * @return Phrase
     */
    public function getHeaderText(): Phrase
    {
        return __('Orders management');
    }

    /**
     * Get Infos RSS and display them in the Shipment Managment
     * Récupère les infos du flux rss et les affiche dans la gestion des expeditions
     * @return string
     */
    public function getDpdfrRss(): string
    {
        $stream       = "";
        $rss_location = 'https://www.dpd.fr/documents/rss/flux.xml';
        if (!simplexml_load_string(file_get_contents($rss_location))) {
            return 'aucune annonce';
        }
        $rss = simplexml_load_string(file_get_contents($rss_location));
        if (empty($rss->channel->item)) {
            return 'aucune annonce';
        }
        foreach ($rss->channel->item as $item) {
            $stream .= '<strong style="color:#dc0032;">' . $item->category . ' > ' . $item->title . ' : </strong> ' . $item->description . '<br/>';
        }
        return $stream !== "" ? $stream : 'aucune annonce';
    }
}
