var config = {
    config: {
        mixins: {
            'Magento_Checkout/js/view/billing-address': {
                'DPDFrance_ShippingM2/js/view/billing-address-mixin': true
            }
        }
    },
    shim: {
        "DPDFrance_ShippingM2/js/view/checkout/shipping/index": ["jquery", "jquery/ui"]
    },
    map: {
        '*': {
            'Magento_Checkout/template/shipping-information/address-renderer/default.html':
                'DPDFrance_ShippingM2/template/shipping-information/address-renderer/default.html',
            'Magento_Checkout/js/model/step-navigator':'DPDFrance_ShippingM2/js/model/step-navigator_mixin',
            'Magento_Checkout/template/billing-address/details.html':
                'DPDFrance_ShippingM2/template/billing-address/details.html'
        }
    }
};
