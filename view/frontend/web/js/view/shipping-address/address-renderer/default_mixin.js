define([
    'ko',
    'Magento_Checkout/js/model/quote',
    'Magento_Customer/js/customer-data'
],function (
    ko,
    quote,
    customerData
) {
    'use strict';

    let countryData = customerData.get('directory-data');

    var mixin = {
        /**
         * @param {String} countryId
         * @return {String}
         */
        getCountryName: function (countryId) {
            return countryData()[countryId] != undefined ? countryData()[countryId].name : ''; //eslint-disable-line
        },
        //remove region
        viewRegion: function () {
            if (quote.shippingMethod().carrier_code == 'dpdrelais' ||
                quote.shippingMethod().carrier_code == 'dpdpredict') {
                return false;
            }

            return true;
        }
    };

    return function (target) {
        return target.extend(mixin);
    };
});
