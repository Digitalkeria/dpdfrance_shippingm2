define(
    [
        'ko',
        'uiComponent',
        'Magento_Checkout/js/action/select-shipping-address',
        'Magento_Checkout/js/model/quote'
    ], function (ko, Component, selectShippingAddressAction, quote) {
        'use strict';
        return Component.extend(
            {
                defaults  : {
                    template: 'DPDFrance_ShippingM2/checkout/sidebar/sidebar_relais'
                },
                initialize: function () {
                    this._super();
                    return this;
                },

                initProperties: function () {
                    this._super();
                    this.isSelected = ko.computed(function () {
                        let isSelected      = false;
                        let shippingAddress = quote.shippingAddress();
                        if (shippingAddress) {
                            isSelected = shippingAddress.getKey() === this.address().getKey();
                        }
                        return isSelected;
                    }, this);
                    return this;
                },

                /** Set selected customer shipping address  */
                selectAddress: function () {
                    selectShippingAddressAction(this.address());
                },
                /** additional logic required for this renderer  **/
            }
        );
    }
);
