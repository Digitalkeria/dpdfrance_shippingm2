define(
    [
        "uiComponent",
        "ko",
        'DPDFrance_ShippingM2/js/view/checkout/relais',
        'Magento_Checkout/js/model/quote'
    ], function (Component, ko, Relais, quote) {
        'use strict';
        return Component.extend(
            {
                address         : Relais.relaisAddress,
                defaults        : {
                    template: 'DPDFrance_ShippingM2/checkout/sidebar/sidebar_relais'
                },
                initialize      : function () {
                    this._super(); //you must call super on components or they will not render
                    Relais.relaisAddress.subscribe(function (a) {});
                    var self = this;
                    quote.shippingAddress.subscribe(function (a) {
                        var test = self;
                    });
                },
                getRelaisAddress: function () {
                    let address2 = Relais.relaisAddress();
                    return address2 ? address2.company + '<br />' + address2.street + ' ' + address2.postcode + ' ' + address2.city : '';
                }
            }
        );
    }
);
