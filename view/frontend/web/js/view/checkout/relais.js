define(
    ['ko'],
    function (ko) {
        'use strict';
        let relaisAddress = ko.observable(null);
        return {
            relaisAddress: relaisAddress
        };
    }
);
