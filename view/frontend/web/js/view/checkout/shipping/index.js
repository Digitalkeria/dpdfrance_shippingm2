define(
    [
        'ko',
        'uiComponent',
        'jquery',
        'domReady!',
        'mage/url',
        'mage/storage',
        'Magento_Checkout/js/checkout-data',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/model/full-screen-loader',
        'Magento_Checkout/js/model/shipping-save-processor',
        'Magento_Checkout/js/action/select-shipping-address',
        'Magento_Checkout/js/action/select-shipping-method',
        'Magento_Checkout/js/action/select-billing-address',
        'Magento_Checkout/js/model/address-converter',
        'Magento_Customer/js/model/customer',
        "Magento_Customer/js/customer-data",
        'DPDFrance_ShippingM2/js/model/service/address',
        'DPDFrance_ShippingM2/js/model/service/customer/manager',

        'uiRegistry',
        'jquery/ui',
    ],
    function (
        ko,
        Component,
        $,
        domReady,
        urlBuilder,
        storage,
        checkoutData,
        quote,
        Loader,
        shippingSaveProcessor,
        selectShippingAddress,
        selectShippingMethod,
        selectBillingAddress,
        converterData,
        customer,
        customerData,
        serviceAddress,
        serviceCustomerManager
    ) {
        'use strict';

        return Component.extend(
            {
                isGuest                      : !customer.isLoggedIn(),                                          // ! Variable pour savoir si l'utilisateur est un visiteur
                shippingMethodObs            : quote.shippingMethod,                                            // ! Quote de la methode de livraison
                shippingAddressObs           : quote.shippingAddress,                                           // ! Quote de l'adresse de livraison
                predictUrl                   : urlBuilder.build("dpdfrance/ajax/predict"),                      // ! URL requête AJAX PREDICT, Recuperation du bloc PREDICT
                predictShippingInformationUrl: urlBuilder.build("dpdfrance/ajax/predictshippinginformation"),   // ! URL requête AJAX PREDICT, sauvegarde des donnees dans la stepData
                relaisUrl                    : urlBuilder.build("dpdfrance/ajax/relais"),                       // ! URL requête AJAX RELAIS, Recuperation du bloc avec le champ recherche de point relais
                relaisListUrl                : urlBuilder.build("dpdfrance/ajax/relaislist"),                   // ! URL requête AJAX RELAIS, Appel au webservice, Recuperation de la liste des 5 suggestions de point relais
                relaisShippingInformationUrl : urlBuilder.build("dpdfrance/ajax/relaisshippinginformation"),    // ! URL requête AJAX RELAIS, sauvegarde des données dans la stepData
                isPredictLoaded              : false,                                                           // ! Variable pour savoir si le bloc predict a été charge
                isPredictPhoneValid          : false,                                                           // ! Variable pour savoir si le numero dans le champs de predict est valide
                isRelaisLoaded               : false,                                                           // ! Variable pour savoir si le bloc relais a été charge
                isRelaisSelected             : false,                                                           // ! Variable pour savoir si le relais a été sélectionné
                customerAddresses            : JSON.parse(JSON.stringify(customer.getShippingAddressList())),   // ! Récupères les adresses enregistrées
                backupAddress                : null,                                                            // ! Sauvegarde de l'adresse initial de l'utilisateur
                currentShippingAddress       : null,                                                            // ! L'adresse de livraison actuelle

                initialize: function () {
                    this._super();
                    let self = this;
                    self.hidePredict();
                    self.hideRelais();
                    serviceAddress.initSources();
                    self.shippingAddressObs.subscribe(function (address) {
                        self.currentShippingAddress = address;
                        address['saveInAddressBook'] = 0;
                        address['save_in_address_book'] = 0;
                        self.backupAddress          = self.makeBackupAddress();
                        self.shippingMethodObs.subscribe(function (m) {
                            // ! Remise a zero des options de livraison
                            self.isPredictLoaded = false;
                            self.isRelaisLoaded  = false;
                            // ! reset current shipping Address | Remise a zero de l'adresse de livraison
                            switch (m && m.method_code) {
                                case 'dpdpredict':
                                    self.source.get('shippingAddress').name = "";
                                    self.loadPredict();
                                    break;
                                case 'dpdrelais':
                                    self.loadRelais();
                                    break;
                                default:
                                    self.source.get('shippingAddress').name = "";
                                    self.isButtonNextDisabled(false);
                                    self.hidePredict();
                                    self.hideRelais();
                                    break;
                            }
                        });
                    });
                },

                /*-------------------------------------------------------------------- * PREDICT ----------------------------------------------------------------------------------*/

                // * Requête AJAX PREDICT
                loadPredict: function () {
                    let self      = this;
                    serviceAddress.resetAddress();
                    serviceAddress.initialize();
                    let telephone = serviceCustomerManager.getShippingAddress().telephone;
                    serviceAddress.setCustomerPhone(telephone);
                    Loader.startLoader();

                    $.ajax(
                        {
                            url  : self.predictUrl,
                            async: false,
                            error: function (j, status, error) {
                                console.error(j);
                                console.error(status);
                                console.error(error);
                            },
                        }
                    ).done(({content}) => {
                        // * Affichage du bloc Predict
                        self.displayPredictBlock(content);
                        // * Affectation du numero de telephone au champ PREDICT
                        if (telephone === "") {
                            telephone = $("input[name='telephone']").val();
                        }
                        let inputTelPredict = $('#gsm_dest');
                        inputTelPredict.val(telephone);
                        // * Verification du numéro de telephone
                        telephone = self.checkTelephone();

                        self.predictErrorPhoneManager(telephone);
                        // * Affichage Comportement des blocs PREDICT et RELAIS
                        self.showPredict();
                        self.hideRelais();
                        Loader.stopLoader();

                        // * Événement au clavier du champ telephone PREDICT
                        inputTelPredict.keyup(function () {
                            telephone = self.checkTelephone();
                            self.predictErrorPhoneManager(telephone);
                        });
                    });
                },
                // * Affichage du bloc Predict
                displayPredictBlock: function (content) {
                    let predictRow   = $('#label_method_dpdpredict_dpdpredict').parent("tr");
                    let blockPredict = $('#dpdpredict_dpdpredict');
                    if (blockPredict.length === 0 && content) {
                        predictRow.after(content);
                    }
                },
                // * Recuperation de la donnee du telephone
                // * Binding du champs de telephone vers le champs de telephone du bloc Predict (One Way)
                checkTelephone: function () {
                    const inputBlockPredict         = $("input#gsm_dest.predict-input-text");
                    const phoneInput                = $('input[name="telephone"]');
                    const blockPredict              = $('#dpdpredict_dpdpredict');
                    const blockLogInUserInfo        = $(".shipping-address-item.selected-item a");
                    let self                        = this;
                    let telephone                   = null;
                    self.isPredictPhoneValid        = false;
                    let valueGuestPhoneInput        = phoneInput.val() === "" ? null : phoneInput.val();                        // * Telephone en mode invite                       | phone number as Guest
                    let valuePredictBlockInput      = inputBlockPredict.val() === "" ? null : inputBlockPredict.val();          // * Telephone si connecté mais sans adresse        | phone number as Log In without registered address
                    let valueLogInUserInfoWithPhone = blockLogInUserInfo.length === 0 ? null : blockLogInUserInfo[0].innerText; // * Telephone si connecté de l'adresse sélectionné | phone number as Log In with registered address

                    telephone = telephone != null ?
                                telephone :
                                (
                                    valueGuestPhoneInput ||         // ! Guest, input form
                                    valueLogInUserInfoWithPhone ||  // ! LogIn User, User info block with phone number
                                    valuePredictBlockInput          // ! LogIn User. predict block input
                                );

                    // * if Predict block is loaded
                    if (blockPredict.length === 1) {
                        // ! Au tape du clavier sur le champ de telephone dans le bloc d'adresse, binding du champ de telephone bloc PREDICT
                        phoneInput.keyup(function () {
                            inputBlockPredict.val(phoneInput.val());
                            telephone                = inputBlockPredict.val();
                            self.isPredictPhoneValid = self.correctTelephone(telephone);
                            self.predictErrorPhoneManager(telephone);
                            return telephone;
                        });
                        // ! Au tape du clavier sur le champ du bloc predict
                        inputBlockPredict.keyup(function () {
                            telephone                = inputBlockPredict.val() !== "" ? inputBlockPredict.val() : phoneInput.val();
                            self.isPredictPhoneValid = self.correctTelephone(telephone);
                            return telephone;
                        });
                    }

                    // * if Predict phone input field value is not empty
                    if (inputBlockPredict.val() !== "") {
                        telephone                = inputBlockPredict.val();
                        self.isPredictPhoneValid = self.correctTelephone(telephone);
                        return telephone;
                    }
                    return telephone;
                },
                // * Verification du formatage du numéro de telephone
                correctTelephone: function (telephone) {
                    if (telephone === null || telephone === "") return false;
                    let gsm_fr  = new RegExp(/^((\+33|0033|0)[67])(?:[ _.-]?(\d{2})){4}$/);
                    let gsm_de  = new RegExp(/^(\+|00)49(\s?\d{10})$/);
                    let gsm_be  = new RegExp(/^(\+|00)32(\s?\d{8})$/);
                    let gsm_at  = new RegExp(/^(\+|00)43(\s?\d{9})$/);
                    let gsm_uk  = new RegExp(/^(\+|00)447([3456789]\d)(\s?\d{7})$/);
                    let gsm_nl  = new RegExp(/^(\+|00)31(\s?\d{9})$/);
                    let gsm_pt  = new RegExp(/^(\+|00)351(\s?\d{9})$/);
                    let gsm_ei  = new RegExp(/^(\+|00)353(\s?\d{8})$/);
                    let gsm_es  = new RegExp(/^(\+|00)34(\s?\d{9})$/);
                    let gsm_it  = new RegExp(/^(\+|00)39(\s?\d{8,11})$/);
                    let gsm_sw  = new RegExp(/^(\+|00)41(\s?\d{9})$/);
                    let gsm_hr  = new RegExp(/^(\+|00)385(\s?\d{8})$/);
                    let gsm_ee  = new RegExp(/^(\+|00)372(\s?\d{7})$/);
                    let gsm_lt  = new RegExp(/^(\+|00)370(\s?\d{8})$/);
                    let gsm_pl  = new RegExp(/^(\+|00)48(\s?\d{9})$/);
                    let gsm_cz  = new RegExp(/^(\+|00)420(\s?\d{9})$/);
                    let gsm_sk  = new RegExp(/^(\+|00)421(\s?\d{9})$/);
                    let gsm_si  = new RegExp(/^(\+|00)386(\s?\d{8})$/);
                    let gsm_lu  = new RegExp(/^(\+|00)352(\s?\d{8})$/);
                    let gsm_le  = new RegExp(/^(\+|00)371(\s?\d{8})$/);
                    let gsm_hu  = new RegExp(/^(\+|00)36(\s?\d{8})$/);
                    let numbers = String(telephone).replace(/ |\.|_|-/g, "");
                    numbers     = numbers.substr(-8);
                    let pattern = ['00000000', '11111111', '22222222', '33333333', '44444444', '55555555', '66666666', '77777777', '88888888', '99999999', '12345678', '23456789', '98765432'];
                    return ((gsm_fr.test(telephone) || gsm_it.test(telephone) || gsm_es.test(telephone) || gsm_ei.test(telephone) || gsm_pt.test(telephone) ||
                             gsm_nl.test(telephone) || gsm_uk.test(telephone) || gsm_at.test(telephone) || gsm_de.test(telephone) || gsm_be.test(telephone) ||
                             gsm_sw.test(telephone) || gsm_hr.test(telephone) || gsm_ee.test(telephone) || gsm_lt.test(telephone) || gsm_pl.test(telephone) ||
                             gsm_cz.test(telephone) || gsm_sk.test(telephone) || gsm_si.test(telephone) || gsm_lu.test(telephone) || gsm_le.test(telephone) ||
                             gsm_hu.test(telephone)) && !pattern.includes(numbers));
                },
                // * Gestion d'erreur du formatage du numéro de telephone et du statut du bouton NEXT et sauvegarde du numero de telephone
                predictErrorPhoneManager: function (telephone) {
                    if (telephone === null) return false;
                    if (this.isPredictPhoneValid) {
                        this.displayPredictPhoneErrorBlock(false);
                        this.stockPredictPhoneNumber(telephone);
                    } else {
                        this.displayPredictPhoneErrorBlock(true);
                    }
                    //Update shipping address
                    serviceAddress.resetAddress();
                    serviceAddress.initialize();
                    serviceAddress.setShippingValue(serviceCustomerManager.getShippingAddress());
                    //Update shipping phone
                    serviceAddress.resetPhone();
                    serviceAddress.setCustomerPhone(telephone);
                    serviceAddress.updatePhone = false;
                },
                // * handle display predict phone error block
                displayPredictPhoneErrorBlock: function (isEnabled) {
                    let blockErrorTelephone = $('#dpdfrance_predict_error');
                    if (isEnabled) {
                        blockErrorTelephone.show();
                        this.isButtonNextDisabled(true);
                    } else {
                        blockErrorTelephone.hide();
                        this.isButtonNextDisabled(false);
                    }
                },
                // * Stockage du numéro de telephone pour Predict
                stockPredictPhoneNumber: function (telephone) {
                    if (telephone === null) {
                        console.error("stockPredictPhoneNumber, probleme avec le numero de telephone")
                        return false;
                    }
                    $.ajax(
                        {
                            url  : this.predictShippingInformationUrl,
                            data : {telephone},
                            async: false,
                            error: function (j, status, error) {
                                console.error(j);
                                console.error(status);
                                console.error(error);
                            },
                        }
                    );
                },

                /*-------------------------------------------------------------------- * RELAIS ----------------------------------------------------------------------------------*/

                // * Requête AJAX RELAIS
                loadRelais: function () {
                    let self = this;
                    Loader.startLoader();
                    // * Get Relais block template and relais suggestion
                    $.ajax(
                        {
                            url  : this.relaisUrl,
                            async: false,
                            error: function (j, status, error) {
                                console.error(j);
                                console.error(status);
                                console.error(error);
                            },
                        }
                    ).done(function (res) {
                        // * Affichage du bloc Relais
                        self.displayRelaisBlock(res);
                    }).done(function () {
                        // * Affecte l'adresse aux champs d'adresse du bloc Relais.
                        if (!self.currentShippingAddress.street) {
                            self.currentShippingAddress = quote.shippingAddress();
                        }

                        // * Insertion de valeurs dans les champs de recherche relais
                        if (self.customerAddresses.length === 0) {
                            // ! Guest or log-in but without address registered
                            $('#address').val($("input[name='street[0]']").val());
                            $('#zipcode').val($("input[name='postcode']").val());
                            $('#city').val($("input[name='city']").val());
                        } else {
                            // ! Log-in with address registered
                            $('#address').val(self.currentShippingAddress.street[0]);
                            $('#zipcode').val(self.currentShippingAddress.postcode);
                            $('#city').val(self.currentShippingAddress.city);
                        }

                        // * Affichage Comportement des blocs PREDICT et RELAIS
                        self.isButtonNextDisabled(true);
                        self.hidePredict();
                        self.showRelais();
                        // * Chargement de la liste des relais
                        self.loadRelaisList();

                        // * Événement au clique du bouton recherche relais
                        $('#dpdfrbutton').click(function () {
                            Loader.startLoader();
                            self.loadRelaisList();
                        });
                    });
                },
                // * Affichage du block Relais
                displayRelaisBlock: function ({content}) {
                    const relaisRow   = $('#label_method_dpdrelais_dpdrelais').parent("tr");
                    const blockRelais = $('#dpdrelais_dpdrelais');
                    // * Si le bloc du relais n'est pas present, affichage du bloc
                    if (blockRelais.length === 0 && relaisRow.length === 1) {
                        relaisRow.after(content);
                    }
                },
                // * Chargement de la liste de relais
                loadRelaisList: function () {
                    this.isRelaisSelected = false;
                    let self              = this;
                    let relaisAddresses   = [];
                    $.ajax(
                        {
                            url  : this.relaisListUrl,
                            data : {
                                'address': $('#address').val(),
                                'zipcode': $('#zipcode').val(),
                                'city'   : $('#city').val(),
                            },
                            async: false,
                            error: function (j, status, error) {
                                console.error(j);
                                console.error(status);
                                console.error(error);
                            },
                        }
                    ).done(function (res) {
                        // * Affichage des relais
                        self.displayRelaisSuggestions(res);
                        // * Vide la liste des suggestions de point relais en cas de refresh ou autre recherche
                        if (relaisAddresses.length > 0) {
                            relaisAddresses.length = 0;
                        }
                        // * Remet les adresses récupérées du call AJAX
                        relaisAddresses = res.addresses;
                        // * Gestion de l'affichage des popups des points relais
                        self.handleRelaisModals(res);
                        Loader.stopLoader();

                        $('.dpdfrrelais_button_ici').click(function () {
                            Loader.startLoader();
                            self.isRelaisSelected = true;
                            //Initialize shipping value into storage
                            serviceAddress.resetAddress();
                            serviceAddress.initialize();
                            serviceAddress.setShippingValue(serviceCustomerManager.getShippingAddress());

                            // * Generation d'une nouvelle instance de l'adresse de facturation
                            let billingAddress               = self.getBillingAddress();
                            let relais_id                    = this.id;
                            let selectedRelaisAddress        = {
                                relais_id: this.id,
                                name     : relaisAddresses[relais_id].name,
                                street   : relaisAddresses[relais_id].street,
                                postcode : relaisAddresses[relais_id].postcode,
                                city     : relaisAddresses[relais_id].city,
                            };
                            let shippingAddress              = self.setShippingAddress(selectedRelaisAddress);
                            quote.shippingAddress().company  = selectedRelaisAddress.name
                            quote.shippingAddress().street   = [selectedRelaisAddress.street];
                            quote.shippingAddress().postcode = selectedRelaisAddress.postcode;
                            quote.shippingAddress().city     = selectedRelaisAddress.city;

                            let checkoutData                     = customerData.get('checkout-data')();
                            checkoutData.shippingAddressFromData = {
                                firstname : shippingAddress.firstname,
                                lastname  : shippingAddress.lastname,
                                company   : selectedRelaisAddress.company,
                                street    : {0: selectedRelaisAddress.street, 1: '', 2: ''},
                                city      : selectedRelaisAddress.city,
                                postcode  : selectedRelaisAddress.postcode,
                                region    : '',
                                region_id : shippingAddress.regionId,
                                country_id: shippingAddress.countryId,
                                fax       : shippingAddress.fax,
                                telephone : shippingAddress.telephone
                            };
                            customerData.set('checkout-data', checkoutData);

                            try {
                                shippingSaveProcessor.saveShippingInformation(shippingAddress.getType());
                            } catch (e) {
                                throw new Error(`Erreur lors de la sauvegarde des infos de livraison ${e.message}`);
                            }

                            try {
                                converterData.quoteAddressToFormAddressData(shippingAddress);
                            } catch (e) {
                                throw new Error(`Erreur de formatage de l'adresse: ${e.message}`);
                            }

                            // * Permet L'affichage du nom du point relais dans le recap de commande
                            try {
                                let source      = self.source.get('shippingAddress');
                                source.name     = selectedRelaisAddress.name;
                                source.street   = [selectedRelaisAddress.street];
                                source.city     = selectedRelaisAddress.city;
                                source.postcode = selectedRelaisAddress.postcode;
                            } catch (e) {
                                throw new Error(`Erreur source: ${e.message}`);
                            }

                            self.stockRelaisIdAndAddress(selectedRelaisAddress);
                            self.isButtonNextDisabled(false);
                            Loader.stopLoader();
                        });
                    });
                },
                // * Affiche la liste des suggestions de point relais
                displayRelaisSuggestions: function ({content, js}) {
                    let suggestionRelais = $('#suggestion');
                    suggestionRelais.empty();
                    suggestionRelais.append(content);
                    suggestionRelais.append(js);
                },
                // * Gestion de la popup des points relais
                handleRelaisModals: function ({relaisCount}) {
                    for (let i = 0; i < relaisCount; i++) {
                        $('#moredetails' + i).click(function () {
                            let i = this.id.replace("moredetails", "");
                            $("#relaydetail" + i).dialog(
                                {
                                    draggable: false,
                                    resizable: false,
                                    modal    : true,
                                    closeText: "Close",
                                    width    : "60%",
                                    height   : 800,
                                    buttons  : {
                                        Fermer: function () {
                                            $(this).dialog("close");
                                        }
                                    }
                                }
                            );
                        });
                    }
                },
                /**
                 * * Get Billing address by generating a new instance
                 * * and set the properties from registered address info or input fields (guest)
                 */
                getBillingAddress: function () {
                    // * Creation d'une nouvelle instance d'une adresse
                    let billingAddress = converterData.formAddressDataToQuoteAddress();
                    // ! Lorsque j'ai une adresse enregistré
                    if (this.customerAddresses.length === 0) {
                        // ! Si je suis un invite ou connecté sans adresse
                        billingAddress.email      = this.isGuest ? $("input[name='username']").val() : this.backupAddress.email;
                        billingAddress.company    = $("input[name='company']").val();
                        billingAddress.lastname   = this.backupAddress !== null ? this.backupAddress.lastname : $("input[name='lastname']").val();
                        billingAddress.firstname  = this.backupAddress !== null ? this.backupAddress.firstname : $("input[name='firstname']").val();
                        billingAddress.middlename = this.backupAddress !== null ? this.backupAddress.middlename : $("input[name='middlename']").val();
                        billingAddress.street     = [$("input[name='street[0]']").val()];
                        billingAddress.postcode   = $("input[name='postcode']").val();
                        billingAddress.city       = $("input[name='city']").val();
                        billingAddress.regionId   = $('select[name="region_id"] option:selected').val() == "" ? null : $('select[name="region_id"] option:selected').val();
                        billingAddress.countryId  = $('select[name="country_id"] option:selected').val();
                        billingAddress.telephone  = $("input[name='telephone']").val();
                    } else {
                        // ! Si je suis connecté qvec adresse
                        billingAddress.customerAddress = this.backupAddress.customerAddress;
                        billingAddress.customerId      = this.backupAddress.customerId;
                        billingAddress.lastname        = this.backupAddress.lastname;
                        billingAddress.firstname       = this.backupAddress.firstname;
                        billingAddress.middlename      = this.backupAddress.middlename;
                        billingAddress.company         = this.backupAddress.company;
                        billingAddress.street          = this.backupAddress.street;
                        billingAddress.postcode        = this.backupAddress.postcode;
                        billingAddress.city            = this.backupAddress.city;
                        billingAddress.regionId        = this.backupAddress.regionId;
                        billingAddress.regionCode      = this.backupAddress.regionCode
                        billingAddress.region          = this.backupAddress.region;
                        billingAddress.countryId       = this.backupAddress.countryId;
                        billingAddress.telephone       = this.backupAddress.telephone;
                        billingAddress.fax             = this.backupAddress.fax;
                    }
                    return billingAddress;
                },
                // * Set Shipping Address
                setShippingAddress: function (selectedRelaisAddress) {
                    let shippingAddress               = converterData.formAddressDataToQuoteAddress();
                    shippingAddress.email             = this.isGuest ? $("input[name='username']").val() : this.currentShippingAddress.email;
                    shippingAddress.customerAddressId = this.currentShippingAddress.customerAddressId;
                    shippingAddress.customerId        = this.currentShippingAddress.customerId;
                    shippingAddress.company           = selectedRelaisAddress.name;
                    shippingAddress.lastname          = this.currentShippingAddress.lastname;
                    shippingAddress.firstname         = this.currentShippingAddress.firstname;
                    shippingAddress.middlename        = this.currentShippingAddress.middlename;
                    shippingAddress.street            = [selectedRelaisAddress.street];
                    shippingAddress.postcode          = selectedRelaisAddress.postcode;
                    shippingAddress.city              = selectedRelaisAddress.city;
                    shippingAddress.region            = this.currentShippingAddress.region;
                    shippingAddress.regionCode        = this.currentShippingAddress.regionCode;
                    shippingAddress.regionId          = this.currentShippingAddress.regionId;
                    shippingAddress.countryId         = this.currentShippingAddress.countryId;
                    shippingAddress.telephone         = this.currentShippingAddress.telephone;
                    shippingAddress.fax               = this.currentShippingAddress.fax;
                    shippingAddress.email             = this.currentShippingAddress.email;
                    shippingAddress.vatId             = this.currentShippingAddress.vatId;
                    shippingAddress.sameAsBilling     = 0;
                    return shippingAddress;
                },
                // * Stockage du relais Id et de l'adresse du relais
                stockRelaisIdAndAddress: function ({relais_id, name, street, postcode, city}) {
                    if (!relais_id || !name || !street || !postcode || !city) {
                        console.error("stockRelaisIdAndAddress, missing datas");
                        return false;
                    }
                    $.ajax(
                        {
                            url  : this.relaisShippingInformationUrl,
                            data : {relais_id, street, postcode, city, name},
                            async: false,
                            error: function (j, status, error) {
                                console.error(j);
                                console.error(status);
                                console.error(error);
                            },
                        }
                    );
                },

                /*-------------------------------------------------------------------- * UTILS ----------------------------------------------------------------------------------*/

                // * Affichage du bloc PREDICT
                showPredict: function () {
                    this.isPredictLoaded = true;
                    $('#dpdpredict_dpdpredict').parent().show();
                },
                // * Cache le bloc PREDICT
                hidePredict: function () {
                    let blockPredict     = $('#dpdpredict_dpdpredict');
                    this.isPredictLoaded = false;
                    if (blockPredict.length === 1) {
                        blockPredict.parent().hide();
                    }
                },
                // * Affichage du bloc Relais
                showRelais: function () {
                    this.isRelaisLoaded = true;
                    $('#dpdrelais_dpdrelais').parent().show();
                },
                // * Cache le bloc Relais
                hideRelais: function () {
                    this.isRelaisLoaded = false;
                    let blockRelais     = $('#dpdrelais_dpdrelais');
                    if (blockRelais.length === 1) {
                        blockRelais.parent().hide();
                    }
                },
                // * Gestion du statut du bouton NEXT
                isButtonNextDisabled: function (enabled = true) {
                    $("#shipping-method-buttons-container button.button.action.continue").prop("disabled", enabled);
                },
                // * Retourne l'adresse initiale si changement de methode de livraison      | Return initial address if shipping method has changed
                // ! Si je suis invite ou login sans adresse, cette fonction retourne rien  | If Guest or User Log In without address, it will return nothing
                getInitialAddress: function () {
                    return this.customerAddresses.filter(shippingAddress => shippingAddress.customerAddressId === this.currentShippingAddress.customerAddressId);
                },
                // * Fais une copie de l'adresse et la renvoie
                makeBackupAddress: function () {
                    return {
                        "customerAddressId": this.currentShippingAddress.customerAddressId,
                        "customerId"       : this.currentShippingAddress.customerId,
                        "company"          : this.currentShippingAddress.company,
                        "lastname"         : this.currentShippingAddress.lastname,
                        "firstname"        : this.currentShippingAddress.firstname,
                        "middlename"       : this.currentShippingAddress.middlename,
                        "street"           : this.currentShippingAddress.street,
                        "postcode"         : this.currentShippingAddress.postcode,
                        "city"             : this.currentShippingAddress.city,
                        "region"           : this.currentShippingAddress.region,
                        "regionCode"       : this.currentShippingAddress.regionCode,
                        "regionId"         : this.currentShippingAddress.regionId,
                        "countryId"        : this.currentShippingAddress.countryId,
                        "telephone"        : this.currentShippingAddress.telephone,
                        "fax"              : this.currentShippingAddress.fax,
                        "email"            : this.currentShippingAddress.email,
                        "vatId"            : this.currentShippingAddress.vatId,
                    };
                },
            });
    }
);
