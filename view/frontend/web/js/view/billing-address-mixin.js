define([
    'ko',
    'Magento_Checkout/js/model/quote'
],function (
    ko,
    quote
) {
    'use strict';

    var mixin = {
        canUseShippingAddress: ko.computed(function () {
            //remove billing from use shipping address
            if (quote.shippingMethod() && (quote.shippingMethod().carrier_code == 'dpdrelais' ||
                quote.shippingMethod().carrier_code == 'dpdpredict')) {
                return false;
            }

            return !quote.isVirtual() && quote.shippingAddress() && quote.shippingAddress().canUseForBilling();
        }),

        viewRegion: function () {
            //remove region from address
            if (quote.shippingMethod().carrier_code == 'dpdrelais' ||
                quote.shippingMethod().carrier_code == 'dpdpredict') {
                return false;
            }

            return true;
        }
    };

    return function (target) {
        return target.extend(mixin);
    };
});
