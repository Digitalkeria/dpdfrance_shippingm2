//This is the storage for variables that's why knockout modifies them
define([
    'jquery',
    'underscore',
    'Magento_Customer/js/customer-data',
    'Magento_Customer/js/model/customer',
    'Magento_Checkout/js/model/quote'
], function (
    $,
    _,
    storage,
    modelCustomer,
    modelQuote
){
    'use strict';

    let cacheKey = 'dpdfrance_service_address',
        selfThis = this,

    /**
     * @param {Object} data
     */
    saveData = function (data) {
        storage.set(cacheKey, data);
    },

    /**
     * @return {*}
     */
    initData = function () {
        //Init storage
        return {
            saveShippingAddress : null,
            customerAddressList : [],
            customerPhone : null,
            updatePhone : true,
            numberShippingAdress : null
        };
    },

    /**
     * @return {*}
     */
    getData = function () {
        //return from storage value by key
        var data = storage.get(cacheKey)();

        if ($.isEmptyObject(data)) {
            data = $.initNamespaceStorage('mage-cache-storage').localStorage.get(cacheKey);

            if ($.isEmptyObject(data)) {
                data = initData();
                saveData(data);
            }
        }

        return data;
    };

    return {
        updatePhone : true,

        initialize: function () {
            //Copy address if customer logged with address
            if (modelCustomer.isLoggedIn()) {
                _.mapObject(modelCustomer.getShippingAddressList(),this.copyAddressList);
            }
        },

        copyAddressList: function(address) {
            let obj = getData();

            if (address && typeof obj.customerAddressList[address.getKey()] == 'undefined') {
                obj.customerAddressList[address.getKey()] = JSON.parse(JSON.stringify(address));
            }

            saveData(obj);
        },

        resetAddress: function () {
            //Update save address if customer logged
            if (modelCustomer.isLoggedIn()) {
                let self = this;
                _.mapObject(modelCustomer.getShippingAddressList(),function (address){
                    let originalAddress = self.getCustomerAddress(address.getKey());
                    if (originalAddress) {
                        address.street = originalAddress.street;
                        address.city = originalAddress.city;
                        address.postcode = originalAddress.postcode;
                        address.telephone = originalAddress.telephone;
                        address.region = originalAddress.region;
                        address.regionCode = originalAddress.regionCode;
                    }
                });
            }
        },

        getCustomerAddress: function (keyAddress) {
            let obj = getData();

            if (modelCustomer.isLoggedIn() && _.size(modelCustomer.getShippingAddressList()) &&
                keyAddress == 'new-customer-address') {
                keyAddress = obj.numberShippingAdress;
            }

            return obj.customerAddressList[keyAddress];
        },

        setShippingValue: function (address) {
            //Save shipping address to storage
            if (address) {
                let obj = getData();
                if (modelCustomer.isLoggedIn() && _.size(modelCustomer.getShippingAddressList()) &&
                    modelQuote.shippingAddress().getKey() != 'new-customer-address') {
                    obj.numberShippingAdress = modelQuote.shippingAddress().getKey();
                }
                obj.saveShippingAddress = JSON.parse(JSON.stringify(address));

                saveData(obj);
            }
        },

        getOriginalShippingValue: function () {
            //get shipping address from storage
            let obj = getData();

            return obj.saveShippingAddress;
        },

        resetPhone: function () {
            if (this.updatePhone) {
                let obj = getData();

                obj.customerPhone = null;

                saveData(obj);
            }
        },

        setCustomerPhone: function (phone) {
            //save customer phone to storage
            if (this.updatePhone) {
                let obj = getData();
                obj.customerPhone = phone;

                saveData(obj);
            }
        },

        getCustomerPhone: function () {
            //Getting the customer's phone number
            let obj = getData();

            if (!obj.customerPhone) {
                if (modelQuote.shippingAddress()) {
                    return modelQuote.shippingAddress().telephone;
                }
            }

            return obj.customerPhone;
        },

        initSources: function () {
            let self = this;
            //Stop update phone after click next step
            jQuery('body').on('click','[data-role="opc-continue"]',function () {
                this.updatePhone = false;
                return true;
            });
            //Update source on click button destination
            jQuery('body').on('keyup',jQuery('#gsm_dest'),function (item) {
                self.setCustomerPhone(item.target.value);
                let obj = getData();
                obj.customerPhone = item.target.value;

                saveData(obj);
            });
        }
    }
})
