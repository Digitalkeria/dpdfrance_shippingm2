define([
    'underscore',
    'Magento_Customer/js/model/customer',
    'DPDFrance_ShippingM2/js/model/service/customer/customer_not_log',
    'DPDFrance_ShippingM2/js/model/service/customer/customer_log'
], function (
    _,
    modelCustomer,
    customerNotLog,
    customerLog
){
    'use strict';

    return {
        getShippingAddress: function () {
            //get shipping address to use in checkout form
            //get shipping address not logged or logged customer without save addresses
            if (!modelCustomer.isLoggedIn() ||
                (modelCustomer.isLoggedIn() && !_.size(modelCustomer.getShippingAddressList()))) {
                return customerNotLog.getShippingAddress();
            }
            //get shipping address logged customer with save addresses
            if (modelCustomer.isLoggedIn() && _.size(modelCustomer.getShippingAddressList())) {
                return customerLog.getShippingAddress();
            }
        }
    }
})
