define([
    'jquery',
    'Magento_Customer/js/customer-data'
], function (
    $,
    customerData
){
    'use strict';

    return {
        getShippingAddress: function () {
            //return original shipping address
            let checkoutDataValue = customerData.get('checkout-data')();
            if (checkoutDataValue.shippingAddressFromData) {
                checkoutDataValue.shippingAddressFromData.company = $("input[name='company']").val();
                checkoutDataValue.shippingAddressFromData.lastname = $("input[name='lastname']").val();
                checkoutDataValue.shippingAddressFromData.firstname = $("input[name='firstname']").val();
                checkoutDataValue.shippingAddressFromData.middlename = $("input[name='middlename']").val();
                checkoutDataValue.shippingAddressFromData.street[0] = $("input[name='street[0]']").val();
                checkoutDataValue.shippingAddressFromData.street[1] = $("input[name='street[1]']").val();
                checkoutDataValue.shippingAddressFromData.street[2] = $("input[name='street[2]']").val();
                checkoutDataValue.shippingAddressFromData.postcode = $("input[name='postcode']").val();
                checkoutDataValue.shippingAddressFromData.city = $("input[name='city']").val();
                checkoutDataValue.shippingAddressFromData.countryId = $('select[name="country_id"] option:selected').val();
                checkoutDataValue.shippingAddressFromData.telephone = $("input[name='telephone']").val();
            }
            return checkoutDataValue.shippingAddressFromData;
        }
    }
})
