define([
    'jquery',
    'Magento_Checkout/js/model/quote',
    'DPDFrance_ShippingM2/js/model/service/address'
], function (
    $,
    modelQuote,
    serviceAddress
){
    'use strict';

    return {
        getShippingAddress: function () {
            //return original shipping address
            return serviceAddress.getCustomerAddress(modelQuote.shippingAddress().getKey());
        }
    }
})
