//This update billing address
define([
    'jquery',
    'underscore',
    'DPDFrance_ShippingM2/js/model/service/address',
    'Magento_Checkout/js/model/quote',
    'Magento_Customer/js/customer-data',
    'Magento_Checkout/js/action/create-billing-address',
    'Magento_Checkout/js/action/select-billing-address',
    'Magento_Checkout/js/checkout-data',
    'Magento_Customer/js/model/customer'
], function (
    $,
    _,
    serviceAddress,
    quote,
    customerData,
    createBillingAddress,
    selectBillingAddress,
    checkoutData,
    modelCustomer
){
    'use strict';

    return {
        updateBillingAddress: function () {
            if (!modelCustomer.isLoggedIn()) {
                this.updateNotLogged();
            }

            if (modelCustomer.isLoggedIn()) {
                this.updateNotLogged();
            }
        },

        updateBillingAddressNotDpd: function () {
            let newBillingAddress = createBillingAddress(quote.shippingAddress());
            // New address must be selected as a billing address
            selectBillingAddress(newBillingAddress);
            checkoutData.setSelectedBillingAddress(newBillingAddress.getKey());
            checkoutData.setNewCustomerBillingAddress(quote.shippingAddress());
        },

        updateLogged: function () {

        },

        updateNotLogged: function () {
            //update billing address not login customer
            let quoteBilling = quote.billingAddress();
            let checkoutDataValue = customerData.get('checkout-data')();
            let originalAddress = serviceAddress.getOriginalShippingValue();

            this.changeAddressFileds(originalAddress, quoteBilling);
            let newBillingAddress = createBillingAddress(originalAddress);
            selectBillingAddress(newBillingAddress);
            checkoutData.setSelectedBillingAddress(newBillingAddress.getKey());
            checkoutData.setNewCustomerBillingAddress(newBillingAddress);
            checkoutDataValue.billingAddressFromData = originalAddress;
        },

        changeAddressFileds: function (originalAddress, changeAddress) {
            //copy address fileds
            if (changeAddress) {
                changeAddress.street = originalAddress.street;
                changeAddress.city = originalAddress.city;
                changeAddress.postcode = originalAddress.postcode;
                changeAddress.telephone = originalAddress.telephone;
                changeAddress.region = originalAddress.region;
                changeAddress.regionCode = originalAddress.regionCode;
            }
        },

        resetBillingAddress: function () {
            serviceAddress.resetPhone();
            //Reset not logged or logged empty save address
            if (!modelCustomer.isLoggedIn() ||
                (modelCustomer.isLoggedIn() && !_.size(modelCustomer.getShippingAddressList()))) {
                this.resetNotLogged();
            }
            //Reset logged customer with save address
            if (modelCustomer.isLoggedIn()) {
                this.resetLogged();
            }
        },

        resetNotLogged: function () {
            let quoteShipping = quote.shippingAddress();
            let checkoutDataValue = customerData.get('checkout-data')();
            let originalAddress = serviceAddress.getOriginalShippingValue();

            this.changeAddressFileds(originalAddress, checkoutDataValue.shippingAddressFromData);
            this.changeAddressFileds(originalAddress, quoteShipping);
            this.changeAddressFileds(originalAddress, checkoutDataValue.billingAddressFromData);
            this.changeAddressFileds(originalAddress, checkoutDataValue.newCustomerBillingAddress);
            this.changeAddressFileds(originalAddress, quote.billingAddress());
            customerData.set('checkout-data', checkoutDataValue);
            //fill form not login customer
            $("input[name='company']").val(originalAddress.company);
            $("input[name='lastname']").val(originalAddress.lastname);
            $("input[name='firstname']").val(originalAddress.firstname);
            $("input[name='street[0]']").val(originalAddress.street[0]);
            $("input[name='street[1]']").val(originalAddress.street[1]);
            $("input[name='street[2]']").val(originalAddress.street[2]);
            $("input[name='postcode']").val(originalAddress.postcode);
            $("input[name='city']").val(originalAddress.city);
            $("input[name='telephone']").val(originalAddress.telephone);
            $("input[name='telephone']").change();
        },

        resetLogged: function () {
            //reset address data from login customer
            let quoteShipping = quote.shippingAddress();
            let checkoutDataValue = customerData.get('checkout-data')();
            let originalAddress = serviceAddress.getOriginalShippingValue();

            this.changeAddressFileds(originalAddress, checkoutDataValue.shippingAddressFromData);
            this.changeAddressFileds(originalAddress, quoteShipping);
            this.changeAddressFileds(originalAddress, checkoutDataValue.billingAddressFromData);
            this.changeAddressFileds(originalAddress, checkoutDataValue.newCustomerBillingAddress);
            this.changeAddressFileds(originalAddress, quote.billingAddress());

            customerData.set('checkout-data', checkoutDataValue);
        }
    }
})
