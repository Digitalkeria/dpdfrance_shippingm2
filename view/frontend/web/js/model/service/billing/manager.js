//This manager modifies billing address
define([
    'Magento_Checkout/js/model/quote',
    'DPDFrance_ShippingM2/js/model/service/billing/relais',
    'DPDFrance_ShippingM2/js/model/service/shipping/predict',
    'DPDFrance_ShippingM2/js/model/service/address',
    'DPDFrance_ShippingM2/js/model/service/customer/manager'
], function (
    quote,
    billingRelais,
    shippingPredict,
    serviceAddress,
    serviceCustomerManager
){
    'use strict';

    return {
        updateBillingAddress: function () {
            //Update billing address
            if (quote.shippingMethod().carrier_code == 'dpdrelais') {
                billingRelais.updateBillingAddress();
            }

            if (quote.shippingMethod().carrier_code == 'dpdpredict') {
                shippingPredict.updatePhoneShipping();
            }
            //If method not dpd new billing address with shipping address
            if (quote.shippingMethod().carrier_code != 'dpdrelais' &&
                quote.shippingMethod().carrier_code != 'dpdpredict') {
                serviceAddress.resetAddress();
                serviceAddress.initialize();
                serviceAddress.setShippingValue(serviceCustomerManager.getShippingAddress());
                billingRelais.updateBillingAddressNotDpd();
            }
        },

        resetAddress: function () {
            //Reset billing address to default address
            billingRelais.resetBillingAddress();
        }
    }
})
