define([
    'Magento_Checkout/js/model/quote',
    'Magento_Customer/js/customer-data',
    'DPDFrance_ShippingM2/js/model/service/address'
], function (
    quote,
    customerData,
    serviceAddress
){
    'use strict';

    return {
        updatePhoneShipping: function () {
            //update phone shipping to predict
            let quoteShipping = quote.shippingAddress();
            let checkoutDataValue = customerData.get('checkout-data')();
            let phone = serviceAddress.getCustomerPhone();

            if (checkoutDataValue.shippingAddressFromData) {
                checkoutDataValue.shippingAddressFromData.telephone = phone;
            }
            quoteShipping.telephone = phone;

            customerData.set('checkout-data', checkoutDataValue);
        },

        getCurrentPhone: function () {

        }
    }
})
