function retour(order_id) {
    let checked = jQuery("#retour_" + order_id).is(":checked");

    jQuery.ajax({
        url : window.location.origin + "/dpdfrance/ajax/advaloremretour",
        data: {
            'order_id': order_id,
            'option'  : 'retour',
            'checked' : checked
        }
    });
}

function advalorem(order_id) {
    let checked = jQuery("#advalorem_" + order_id).is(":checked");

    jQuery.ajax({
        url : window.location.origin + "/dpdfrance/ajax/advaloremretour",
        data: {
            'order_id': order_id,
            'option'  : 'advalorem',
            'checked' : checked
        }
    });
}
