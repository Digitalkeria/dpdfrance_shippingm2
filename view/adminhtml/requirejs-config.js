var config = {
    deps: [
        "DPDFrance_ShippingM2/js/view/grid/index"
    ],
    shim: {
        "DPDFrance_ShippingM2/js/view/grid/index": ["jquery", "jquery/ui", "mage/url"]
    }
};