<?php

namespace DPDFrance\ShippingM2\Helper\Adminhtml\Carrier;

/**
 * Cette classe récupère la configuration de la methode Relais
 */
class DPDRelaisHelper extends AbstractCarrierHelper
{
    private static $instance;

    public function __construct()
    {
        parent::__construct("dpdrelais");
    }

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function getUrlWebservice()
    {
        return $this->getValue("carriers/dpdrelais/url_webservice_dpdrelais");
    }

    public function getIdMarchand()
    {
        return $this->getValue("carriers/dpdrelais/id_marchand");
    }

    public function getSecurityKey()
    {
        return $this->getValue("carriers/dpdrelais/security_key");
    }

    public function getGoogleMapsApiKey()
    {
        return $this->getValue("carriers/dpdrelais/googlemaps_apikey");
    }

    public function getAll()
    {
        $data                    = parent::getAll();
        $data["url web service"] = $this->getUrlWebservice();
        $data["id marchand"]     = $this->getIdMarchand();
        $data["security key"]    = $this->getSecurityKey();
        $data["google maps"]     = $this->getGoogleMapsApiKey();
        return $data;
    }
}
