<?php

namespace DPDFrance\ShippingM2\Helper\Adminhtml\Carrier;

/**
 * Cette classe récupère la configuration commune aux 3 méthodes
 */
abstract class AbstractCarrierHelper extends \DPDFrance\ShippingM2\Helper\Adminhtml\AbstractBaseHelper
{
    private $method;

    public function __construct($method)
    {
        parent::__construct();
        $this->method = $method;
    }

    public function isActive()
    {
        return $this->getValue("carriers/" . $this->method . "/active") === "1";
    }

    public function getTitle()
    {
        return $this->getValue("carriers/" . $this->method . "/title");
    }

    public function getName()
    {
        return $this->getValue("carriers/" . $this->method . "/name");
    }

    public function getAgenceDPD()
    {
        return $this->getValue("carriers/" . $this->method . "/agence_dpd");
    }

    public function getContract()
    {
        return $this->getValue("carriers/" . $this->method . "/contract");
    }

    public function getShippingCosts()
    {
        return $this->getValue("carriers/" . $this->method . "/shipping_costs");
    }

    public function showWhenUnavailable()
    {
        return $this->getValue("carriers/" . $this->method . "/show_when_unavailable") === "1";
    }

    public function getErrorMessage()
    {
        return $this->getValue("carriers/" . $this->method . "/error_message");
    }

    public function getSortOrder()
    {
        return $this->getValue("carriers/" . $this->method . "/sort_order");
    }

    public function getAll()
    {
        $data = array(
            'active'           => $this->isActive(),
            'title'            => $this->getTitle(),
            'name'             => $this->getName(),
            'agence dpd'       => $this->getAgenceDPD(),
            'contract'         => $this->getContract(),
            'shipping costs'   => $this->getShippingCosts(),
            'show unavailable' => $this->showWhenUnavailable(),
            'error message'    => $this->getErrorMessage(),
            'sortOrder'        => $this->getSortOrder()
        );

        return $data;
    }
}
