<?php

namespace DPDFrance\ShippingM2\Helper\Adminhtml\Carrier;

/**
 * Cette classe récupère la configuration de la methode Classic
 */
class DPDClassicHelper extends AbstractCarrierHelper
{
    private static $instance;

    public function __construct()
    {
        parent::__construct("dpdclassic");
    }

    /**
     * @return \DPDFrance\ShippingM2\Helper\Adminhtml\Carrier\DPDClassicHelper
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function getAll()
    {
        return parent::getAll();
    }
}
