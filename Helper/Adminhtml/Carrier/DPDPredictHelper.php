<?php

namespace DPDFrance\ShippingM2\Helper\Adminhtml\Carrier;

/**
 * Cette classe récupère la configuration de la methode Predict
 */
class DPDPredictHelper extends AbstractCarrierHelper
{
    private static $instance;

    public function __construct()
    {
        parent::__construct("dpdpredict");
    }

    /**
     * @return \DPDFrance\ShippingM2\Helper\Adminhtml\Carrier\DPDPredictHelper
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

}
