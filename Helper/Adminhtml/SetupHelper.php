<?php

namespace DPDFrance\ShippingM2\Helper\Adminhtml;

class SetupHelper extends \DPDFrance\ShippingM2\Helper\Adminhtml\AbstractBaseHelper
{
    private static $instance;

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function getName()
    {
        return $this->getValue("dpdfrance_configuration/shipping_information/name");
    }

    public function getAddress1()
    {
        return $this->getValue("dpdfrance_configuration/shipping_information/address1");
    }

    public function getAddress2()
    {
        return $this->getValue("dpdfrance_configuration/shipping_information/address2");
    }

    public function getZipCode()
    {
        return $this->getValue("dpdfrance_configuration/shipping_information/zip_code");
    }

    public function getCity()
    {
        return $this->getValue("dpdfrance_configuration/shipping_information/city");
    }

    public function getPays()
    {
        return $this->getValue("dpdfrance_configuration/shipping_information/country");
    }

    public function getPhoneNumber()
    {
        return $this->getValue("dpdfrance_configuration/shipping_information/phone_number");
    }

    public function getMobile()
    {
        return $this->getValue("dpdfrance_configuration/shipping_information/mobile");
    }

    public function getEmail()
    {
        return $this->getValue("dpdfrance_configuration/shipping_information/email");
    }

    public function getAssurance()
    {
        return $this->getValue("dpdfrance_configuration/shipping_gestion/assurance");
    }

    public function getRetour()
    {
        return $this->getValue("dpdfrance_configuration/shipping_gestion/retour");
    }

    public function phoneNumberIsCorrect()
    {
        $phone_number   = $this->getPhoneNumber();
        $code_pays_dest = $this->getPays();
        return formatGSM($phone_number, $code_pays_dest);
        //return false;
    }

    public function mobileIsCorrect()
    {
        $phone_number   = $this->getMobile();
        $code_pays_dest = $this->getPays();
        return self::formatGSM($phone_number, $code_pays_dest);
    }

    /* Format GSM */
    public static function formatGSM($tel_dest, $code_pays_dest)
    {
        $tel_dest = str_replace(array(' ', '.', '-', ',', ';', '/', '\\', '(', ')'), '', $tel_dest);
        // Chrome autofill fix
        if (Tools::substr($tel_dest, 0, 2) == 33) {
            $tel_dest = substr_replace($tel_dest, '0', 0, 2);
        }
        switch ($code_pays_dest) {
            case 'FR':
                return preg_match('/^((\+33|0|0033)[67])(?:[ _.-]?(\d{2})){4}$/', $tel_dest) ? $tel_dest : false;
                break;

            case 'DE':
                return preg_match('/^(\+|00)49(15|16|17)(\s?\d{7,8})$/', $tel_dest) ? $tel_dest : false;
                break;

            case 'BE':
                return preg_match('/^(\+|00)324([56789]\d)(\s?\d{6})$/', $tel_dest) ? $tel_dest : false;
                break;

            case 'AT':
                return preg_match('/^(\+|00)436([56789]\d)(\s?\d{4})$/', $tel_dest) ? $tel_dest : false;
                break;

            case 'GB':
                return preg_match('/^(\+|00)447([3456789]\d)(\s?\d{7})$/', $tel_dest) ? $tel_dest : false;
                break;

            case 'NL':
                return preg_match('/^(\+|00)316(\s?\d{8})$/', $tel_dest) ? $tel_dest : false;
                break;

            case 'PT':
                return preg_match('/^(\+|00)3519(\s?\d{7})$/', $tel_dest) ? $tel_dest : false;
                break;

            case 'IE':
                return preg_match('/^(\+|00)3538(\s?\d{8})$/', $tel_dest) ? $tel_dest : false;
                break;

            case 'ES':
                return preg_match('/^(\+|00)34(6|7)(\s?\d{8})$/', $tel_dest) ? $tel_dest : false;
                break;

            case 'IT':
                return preg_match('/^(\+|00)393(\s?\d{9})$/', $tel_dest) ? $tel_dest : false;
                break;

            default:
                return $tel_dest;
                break;
        }
    }

    public function getAll()
    {
        return [
            'name'           => $this->getName(),
            'adress1'        => $this->getAddress1(),
            'address2'       => $this->getAddress2(),
            'zipcode'        => $this->getZipCode(),
            'city'           => $this->getCity(),
            'phonenumber'    => $this->getPhoneNumber(),
            'mobile'         => $this->getMobile(),
            'email'          => $this->getEmail(),
            'assurance'      => $this->getAssurance(),
            'retour'         => $this->getRetour(),
            'phone correct'  => $this->phoneNumberIsCorrect(),
            'mobile correct' => $this->mobileIsCorrect()
        ];
    }
}
