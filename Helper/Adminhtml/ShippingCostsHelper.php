<?php

namespace DPDFrance\ShippingM2\Helper\Adminhtml;

/**
 * Cette classe servira pour la gestion des tarifs et sera utilisée par toutes les méthodes
 */
class ShippingCostsHelper
{
    const CONF = 'test';
    private $_json;
    private $_country_id;
    private $_postcode;
    private $_weight;
    private $_price;
    private $_entities;
    private $_cart = "weight";
    private $quote;

    /**
     * Prend en paramètre le paramètre json d'une des méthodes
     */
    public function __construct($json_string, $country_id, $postcode, $weight, $price, $quantity, $quote = null)
    {
        $this->json        = json_decode($json_string);
        $this->_country_id = $country_id;
        $this->_postcode   = $postcode;
        $this->_weight     = $weight;
        $this->_price      = $price;
        $this->_quantity   = $quantity;
        $this->quote       = $quote;
        $this->_entities   = [];

        foreach ($this->json as $entity) {
            $this->_entities[] = $this->form($entity);
        }

        $this->filter();
    }

    private function form($entity)
    {
        if (isset($entity->conditions)) {
            if (strpos($entity->conditions, 'cart.price_including_tax') !== false) {
                $entity->conditions = str_replace("{cart.price_including_tax}>=", "", $entity->conditions);
            }

            if (strpos($entity->conditions, 'cart.price_excluding_tax') !== false) {
                $entity->conditions = str_replace("{cart.price_excluding_tax}>=", "", $entity->conditions);
            }

            $entity->conditions = (int)$entity->conditions;
        }

        // * Exclusion des villes
        if (preg_match("/-\([a-zA-Z0-9,]*\)/", $entity->shipto, $exclude)) {
            $exclude         = $exclude[0];
            $entity->shipto  = str_replace($exclude, "", $entity->shipto);
            $exclude         = trim($exclude, "-(");
            $exclude         = trim($exclude, ")");
            $entity->exclude = explode(",", $exclude);
        }

        // * Frais selon les villes
        if (preg_match("/\([a-zA-Z0-9,*]*\)/", $entity->shipto, $cities)) {
            $cities         = $cities[0];
            $entity->shipto = str_replace($cities, "", $entity->shipto);
            $cities         = trim($cities, "(");
            $cities         = trim($cities, ")");
            $entity->cities = explode(",", $cities);
        }

        $entity->countries = explode(",", $entity->shipto);
        unset($entity->shipto);


        if (strpos($entity->fees, "+{") !== false) {
            $entity->base_fee = (float)explode("+{", $entity->fees)[0];
        } elseif (strpos($entity->fees, "{") === false) {
            $entity->base_fee = (float)$entity->fees;
            $entity->fees     = [];
            return $entity;
        } else {
            $entity->base_fee = 0;
        }

        // * Frais me basant sur la quantité ou sur le poids
        $this->_cart = strpos($entity->fees, 'cart.quantity') !== FALSE ? 'quantity' : 'weight';

        preg_match("/in[0-9.: ,]*}/", $entity->fees, $entity->fees);
        $entity->fees = $entity->fees[0];
        $entity->fees = trim($entity->fees, "in ");
        $entity->fees = trim($entity->fees, "}");
        $entity->fees = explode(", ", $entity->fees);
        $fees         = [];
        $i            = 0;
        foreach ($entity->fees as $fee) {
            $fee    = explode(":", $fee);
            $first  = (float)$fee[0];
            $second = (float)$fee[1];
            $fees[] = [$i, $first, $second];
            $i      = (float)$first;
        }

        $entity->fees = $fees;
        return $entity;
    }

    private function filter()
    {
        $this->removeEntitiesByCountry();
        $this->removeEntitiesByNot();
        $this->removeEntitiesByConditions();
        $this->removeEntitiesByCities();
    }

    /**
     * Supprime le pays
     */
    private function removeEntitiesByCountry()
    {
        $e = [];
        foreach ($this->_entities as $entity) {
            foreach ($entity->countries as $country_id) {
                if ($this->_country_id === $country_id) {
                    $e[] = $entity;
                    break;
                }
            }
        }
        $this->_entities = $e;
    }

    private function removeEntitiesByNot()
    {
        $e = [];

        foreach ($this->_entities as $entity) {
            if (!isset($entity->exclude)) {
                $e[] = $entity;
                continue;
            }

            foreach ($entity->exclude as $exclude) {
                if (strpos($this->_postcode, "20") === 0 && ($exclude === "2A" || $exclude === "2B")) {
                } else {
                    $e[] = $entity;
                    break;
                }
            }
        }

        $this->_entities = $e;
    }

    private function removeEntitiesByConditions()
    {
        $e = [];
        foreach ($this->_entities as $entity) {
            if (!isset($entity->conditions)) {
                continue;
            }
            if ($this->_price >= $entity->conditions) {
                $e[] = $entity;
            }
        }

        if (count($e) === 0) {
            foreach ($this->_entities as $entity) {
                if (!isset($entity->conditions)) {
                    $e[] = $entity;
                }
            }
        }
        $this->_entities = $e;
    }

    private function removeEntitiesByCities()
    {
        $e = [];

        foreach ($this->_entities as $entity) {
            if (!isset($entity->cities)) {
                continue;
            }

            foreach ($entity->cities as $city) {
                if ($this->_postcode === $city) {
                    $e[] = $entity;
                    break;
                }

                if (strpos($city, "*") !== false) {
                    $first  = explode("*", $city)[0];
                    $second = substr($this->_postcode, 0, 2);

                    if ($first == $second) {
                        $e[] = $entity;
                    }
                }
            }
        }

        if (count($e) === 0) {
            foreach ($this->_entities as $entity) {
                if (!isset($entity->cities)) {
                    $e[] = $entity;
                }
            }
        }

        $this->_entities = $e;
    }

    public function toString()
    {
        echo "<h1>Country = " . $this->_country_id . "</h1>";
        echo "<h1>Postcode = " . $this->_postcode . "</h1>";
        echo "<h1>Weight = " . $this->_weight . "</h1>";
        echo "<h1>Price = " . $this->_price . "</h1>";

        $i = 1;

        foreach ($this->_entities as $entity) {
            echo "<h1>" . $i . "</h1>";
            echo "<pre>";
            echo "</pre><br><br><br>";

            $i++;
        }

        echo "<h1>" . $this->getLabel() . "</h1>";
        echo "<h1>Result livraison = " . $this->getPrice() . " €</h1>";
        echo "<h1>Result commande entière = " . ($this->getPrice() + $this->_price) . " €</h1>";

        die();
    }

    public function getLabel()
    {
        if (!empty($this->_entities[0]->label)) {
            return $this->_entities[0]->label;
        }

        return '';
    }

    /**
     * Recuperation du prix
     * @return false|int|void
     */
    public function getPrice()
    {
        // * Si base fee a 0 si la base fee de l'entité est vide
        $base_fee = !empty($this->_entities[0]->base_fee) ? $this->_entities[0]->base_fee : 0;

        // * Recuperation des frais selon si on juge par rapport à la quantité ou au poids
        $fee = $this->_cart === 'quantity' ? $this->getFeeByQuantity() : $this->getFeeByWeight();

        return $base_fee + $fee;
    }

    /**
     * Recuperation des frais selon la quantité
     * @return false|void
     */
    private function getFeeByQuantity()
    {
        if (empty($this->_entities[0])) {
            return false;
        }
        $entity = $this->_entities[0];
        foreach ($entity->fees as $fee) {
            $min = $fee[0];
            $max = $fee[1];
            if ($this->_quantity > $min && $this->_quantity <= $max) {
                return $fee[2];
            }
        }
    }

    /**
     * Recuperation des frais selon le poids
     * @return false|void
     */
    private function getFeeByWeight()
    {
        if (empty($this->_entities[0])) {
            return false;
        }
        $entity = $this->_entities[0];

        foreach ($entity->fees as $fee) {
            $min = $fee[0];
            $max = $fee[1];

            if ($this->_weight > $min && $this->_weight <= $max) {
                return $fee[2];
            }
        }
    }

    /**
     * @return array
     */
    private function removeEntitiesByCouponCode()
    {
        $e = [];
        foreach ($this->_entities as $entity) {
            if (!isset($entity->coupon)) {
                continue;
            }
            if (strpos($entity->coupon, '{cart.coupon}') !== false) {
                $entity->coupon = str_replace('{cart.coupon}==', '', $entity->coupon);
            }
            if ($entity->coupon == $this->getByCoupon()) {
                $e[] = $entity;
            }
            if (count($e)) {
                $this->_entities = $e;
            }
        }

        return $this->_entities;
    }

    private function getByCoupon()
    {
        if ($this->quote) {
            return $this->quote->getCouponCode();
        }
    }

    private function removeEntitiesByQuantity()
    {
        $e = [];
        foreach ($this->_entities as $entity) {
            if (!isset($entity->quantity)) {
                continue;
            }

            if (strpos($entity->quantity, '{cart.quantity}') !== false) {
                $entity->quantity = (int)str_replace('{cart.quantity}<=', '', $entity->quantity);
            }

            if ($entity->quantity <= $this->getCartQuantity()) {
                $e[] = $entity;
            }
            if (count($e)) {
                $this->_entities = $e;
            }

            return $this->_entities;
        }
    }

    private function getCartQuantity()
    {
        if ($this->quote) {
            return (int)$this->quote->getItemsQty();
        }
    }

    private function removeEntitiesByProductAttribute()
    {
    }
}
