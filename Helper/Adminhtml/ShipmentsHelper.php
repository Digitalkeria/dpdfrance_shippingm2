<?php

namespace DPDFrance\ShippingM2\Helper\Adminhtml;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\ResourceConnection;

/**
 * Cette classe sera utilisee pour la gestion des expeditions et interagira avec la base de donnees
 */
class ShipmentsHelper
{
    private static $instance;

    /**
     * @var ResourceConnection
     */
    protected $_resource;

    public function __construct()
    {
        $objectManager   = ObjectManager::getInstance();
        $this->_resource = $objectManager->get(ResourceConnection::class);
    }

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function insertInDPDOption($order_id, $advalorem, $retour)
    {
        $table = $this->_resource->getTableName('dpd_option');
        $sql   = "insert into " . $table . " (order_id, advalorem, retour) values ('" . $order_id . "', " . $advalorem . ", '" . $retour . "')";

        $this->_resource->getConnection()->query($sql);
    }

    public function updateInDPDOptionAdvalorem($order_id, $advalorem)
    {
        $table = $this->_resource->getTableName('dpd_option');
        $sql   = "update " . $table . " set advalorem = " . $advalorem . " where order_id = '" . $order_id . "'";
        $this->_resource->getConnection()->query($sql);
    }

    /**
     * Modify All AdValorem settings to 1 or 0
     *
     * @param string $advalorem
     */
    public function updateInDPDOptionAdValoremSettings(string $advalorem): void
    {
        $table = $this->_resource->getTableName('dpd_option');
        $sql   = "update " . $table . " set advalorem = " . $advalorem;
        $this->_resource->getConnection()->query($sql);
    }

    public function updateInDPDOptionRetour($order_id, $retour)
    {
        $table = $this->_resource->getTableName('dpd_option');
        $sql   = "update " . $table . " set retour = " . $retour . " where order_id = '" . $order_id . "'";
        $this->_resource->getConnection()->query($sql);
    }

    /**
     * Modify All Retour settings to 1 or 0
     *
     * @param string $retour
     */
    public function updateInDPDOptionRetourSettings(string $retour): void
    {
        $table = $this->_resource->getTableName('dpd_option');
        $sql   = "update " . $table . " set retour = " . $retour;
        $this->_resource->getConnection()->query($sql);
    }

    public function setOrderProcessing($order_id)
    {
        $objectManager = ObjectManager::getInstance();
        $order         = $objectManager->create('\Magento\Sales\Model\Order')->load($order_id);
        $order->setState("processing")->setStatus("processing");
        $order->save();
    }

    public function getAdvalorem($order_id)
    {
        $table     = $this->_resource->getTableName('dpd_option');
        $sql       = "select advalorem from dpd_option where order_id = '" . $order_id . "'";
        $assurance = $this->_resource->getConnection()->query($sql)->fetchAll();
        return $assurance[0]['advalorem'];
    }

    public function getRetour($order_id)
    {
        $table  = $this->_resource->getTableName('dpd_option');
        $sql    = "select retour from dpd_option where order_id = '" . $order_id . "'";
        $retour = $this->_resource->getConnection()->query($sql)->fetchAll();
        return $retour[0]['retour'];
    }

    public function deleteFromDPDOptionCompleteOrder()
    {
        $table = $this->_resource->getTableName('dpd_option');
        $sql   = "delete from dpd_option where order_id in (select entity_id from sales_order s where status = 'complete');";
        $this->_resource->getConnection()->query($sql);
    }
}
