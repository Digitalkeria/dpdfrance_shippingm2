<?php

namespace DPDFrance\ShippingM2\Helper\Adminhtml;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\ObjectManager;

/**
 * Classe de base de tous les helpers
 */
abstract class AbstractBaseHelper extends AbstractHelper
{
    private $scope_config;

    public function __construct()
    {
        $object_manager     = ObjectManager::getInstance();
        $this->scope_config = $object_manager->get(ScopeConfigInterface::class);
    }

    protected function getValue($path)
    {
        return $this->scope_config->getValue($path);
    }
}
