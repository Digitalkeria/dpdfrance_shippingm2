<?php

namespace DPDFrance\ShippingM2\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Choix pour les 3 méthodes dans l'admin concernant l'assurance en combobox
 */
class Assurance implements ArrayInterface
{
    public function toOptionArray()
    {
        return [
            0 => "Assurance intégrée (33€ / kg transporté - cdt. LOTI)",
            1 => "Assurance complémentaire Ad Valorem"
        ];
    }
}
