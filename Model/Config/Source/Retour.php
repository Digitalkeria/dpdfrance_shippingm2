<?php

namespace DPDFrance\ShippingM2\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Choix pour les 3 méthodes dans l'admin concernant le Retour en combobox
 */
class Retour implements ArrayInterface
{
    public function toOptionArray() {
        return [
            0 => "Aucun retour",
            3 => "Sur demande",
            4 => "Préparée"
        ];
    }
}
