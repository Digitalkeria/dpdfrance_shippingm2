<?php

namespace DPDFrance\ShippingM2\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Choix pour les 3 méthodes dans l'admin concernant le Oui/Non en combobox
 */
class OuiNon implements ArrayInterface
{
    public function toOptionArray() {
        return [
            true  => "Oui",
            false => "Non"
        ];
    }
}
