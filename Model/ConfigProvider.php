<?php

namespace DPDFrance\ShippingM2\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\View\LayoutInterface;
use Magento\Cms\Block\Block;

class ConfigProvider implements ConfigProviderInterface
{
    /** @var LayoutInterface */
    protected $_layout;

    public function __construct(LayoutInterface $layout)
    {
        $this->_layout = $layout;
    }

    public function getConfig()
    {
        $myBlockId = "my_static_block"; // CMS Block Identifier
        //$myBlockId = 20; // CMS Block ID

        return [
            'my_block_content' => $this->_layout->createBlock(Block::class)->setBlockId($myBlockId)->toHtml()
        ];
    }
}
