<?php

namespace DPDFrance\ShippingM2\Model;

use Magento\Checkout\Model\ConfigProviderInterface;

class AdditionalConfigProvider implements ConfigProviderInterface
{
   public function getConfig()
   {
       $output['test_config'] = 'Test Config';
       return $output;
   }
}
