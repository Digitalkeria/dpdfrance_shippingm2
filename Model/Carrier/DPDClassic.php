<?php

namespace DPDFrance\ShippingM2\Model\Carrier;

use DPDFrance\ShippingM2\Helper\Adminhtml\Carrier\DPDClassicHelper;
use DPDFrance\ShippingM2\Helper\Adminhtml\ShippingCostsHelper;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ScopeInterface;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory;
use Magento\Quote\Model\Quote\Address\RateResult\MethodFactory;
use Magento\Shipping\Model\Carrier\AbstractCarrier;
use Magento\Shipping\Model\Carrier\CarrierInterface;
use Magento\Shipping\Model\Rate\ResultFactory;
use Psr\Log\LoggerInterface;

class DPDClassic extends AbstractCarrier implements CarrierInterface
{
    private $dpdclassic_helper;
    const CODE = 'dpdclassic';
    protected $_code      = self::CODE;
    protected $_debugData = [];
    protected $_rateResultFactory;
    protected $_rateMethodFactory;
    private   $checkoutSession;

    public function __construct(
        CheckoutSession      $checkoutSession,
        ScopeConfigInterface $scopeConfig,
        ErrorFactory         $rateErrorFactory,
        LoggerInterface      $logger,
        ResultFactory        $rateResultFactory,
        MethodFactory        $rateMethodFactory,
        array                $data = []
    )
    {
        $this->_rateResultFactory = $rateResultFactory;
        $this->_rateMethodFactory = $rateMethodFactory;
        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);
        $this->dpdclassic_helper = DPDClassicHelper::getInstance();
        $this->checkoutSession   = $checkoutSession;
    }

    /**
     * $request for items and config
     * Shipping price calculation
     * Devra utiliser ShippingCostsHelper pour la gestion des tarifs
     * @param RateRequest $request
     * @return false|\Magento\Shipping\Model\Rate\Result
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function collectRates(RateRequest $request)
    {
        if ($this->dpdclassic_helper->isActive() === false) {
            return false;
        }

        $quote  = $this->checkoutSession->getQuote();
        $result = $this->_rateResultFactory->create();
        $method = $this->_rateMethodFactory->create();
        $method->setCarrier('dpdclassic');
        $method->setCarrierTitle($this->dpdclassic_helper->getTitle());
        $country_id  = $request->getDestCountryId();
        $postcode    = $request->getDestPostcode();
        $weight      = $request->getPackageWeight();
        $price       = $request->getPackageValue();
        $quantity    = $request->getPackageQty();
        $json_string = DPDClassicHelper::getInstance()->getShippingCosts();

        //condition pays par la liste de prix
        $prcoid = 0;
        $json   = json_decode($json_string);
        foreach ($json as $entity) {
            if (preg_match("/-\([a-zA-Z0-9,]*\)/", $entity->shipto, $exclude)) {
                $exclude        = $exclude[0];
                $entity->shipto = str_replace($exclude, "", $entity->shipto);
            }
            $countries = explode(",", $entity->shipto);
            foreach ($countries as $pricecountry_id) {
                if ($pricecountry_id == $country_id) {
                    $prcoid = 1;
                    break;
                }
            }
        }

        if ($prcoid === 0) {
            return false;
        }

        $shippingCostsHelper = new ShippingCostsHelper($json_string, $country_id, $postcode, $weight, $price, $quantity, $quote);

//        $f = fopen("test_classic.txt", "w");
//        fwrite($f, print_r($quote) . " ");
//        fwrite($f, $postcode . " ");
//        fwrite($f, $weight . " ");
//        fwrite($f, $price);
//        fclose($f);

        $method->setPrice($shippingCostsHelper->getPrice());
        $method->setMethod('dpdclassic');
        $method->setCarrierTitle($this->dpdclassic_helper->getTitle());
        $method->setMethodTitle($this->dpdclassic_helper->getName());
        $result->append($method);

        /* If total cart weight over 30 kg or 66 lbs, don't display shipping method*/
        $total_weight = $this->getTotalWeight();
        $weightUnit   = $this->_scopeConfig->getValue('general/locale/weight_unit', ScopeInterface::SCOPE_DEFAULT);
        if (($total_weight > 30 && $weightUnit === 'kgs') || ($total_weight > 65 && $weightUnit === 'lbs')) {
            return false;
        }

        return $result;
    }

    /**
     * Get Total Weight of the cart
     * @return float|int
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getTotalWeight()
    {
        $items        = $this->checkoutSession->getQuote()->getAllItems();
        $total_weight = 0;
        foreach ($items as $item) {
            $total_weight += ($item->getWeight() * $item->getQty());
        }

        return $total_weight;
    }

    /**
     * @return array
     */
    public function getAllowedMethods()
    {
        return ['dpdclassic' => $this->dpdclassic_helper->getName()];
    }
}
