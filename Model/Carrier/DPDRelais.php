<?php

namespace DPDFrance\ShippingM2\Model\Carrier;

use DPDFrance\ShippingM2\Helper\Adminhtml\Carrier\DPDRelaisHelper;
use DPDFrance\ShippingM2\Helper\Adminhtml\ShippingCostsHelper;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ScopeInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory;
use Magento\Quote\Model\Quote\Address\RateResult\MethodFactory;
use Magento\Shipping\Model\Carrier\AbstractCarrier;
use Magento\Shipping\Model\Carrier\CarrierInterface;
use Magento\Shipping\Model\Rate\Result;
use Magento\Shipping\Model\Rate\ResultFactory;
use Psr\Log\LoggerInterface;

class DPDRelais extends AbstractCarrier implements CarrierInterface
{
    const CODE = 'dpdrelais';
    protected $_code = self::CODE;
    private   $dpdrelais_helper;
    protected $_rateResultFactory;
    protected $_rateMethodFactory;
    private   $checkoutSession;

    public function __construct(
        CheckoutSession      $checkoutSession,
        ScopeConfigInterface $scopeConfig,
        ErrorFactory         $rateErrorFactory,
        LoggerInterface      $logger,
        ResultFactory        $rateResultFactory,
        MethodFactory        $rateMethodFactory,
        array                $data = []
    )
    {
        $this->_rateResultFactory = $rateResultFactory;
        $this->_rateMethodFactory = $rateMethodFactory;
        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);
        $this->dpdrelais_helper = DPDRelaisHelper::getInstance();
        $this->checkoutSession  = $checkoutSession;
    }

    /**
     * $request for items and config
     * Shipping price calculation
     * Devra utiliser ShippingCostsHelper pour la gestion des tarifs
     * @param RateRequest $request
     * @return bool|Result|Result
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function collectRates(RateRequest $request)
    {
        if ($this->dpdrelais_helper->isActive() === false) {
            return false;
        }

        $country_id = $request->getDestCountryId();
        $postcode   = $request->getDestPostcode();
        $weight     = $request->getPackageWeight();
        $price      = $request->getPackageValue();
        $quantity   = $request->getPackageQty();

        if ($country_id !== 'FR' && $country_id !== 'MC') {
            return false;
        }

        $quote  = $this->checkoutSession->getQuote();
        $result = $this->_rateResultFactory->create();
        $method = $this->_rateMethodFactory->create();
        $method->setCarrier('dpdrelais');
        $method->setCarrierTitle($this->dpdrelais_helper->getTitle());
        $method->setMethod('dpdrelais');
        $method->setMethodTitle($this->dpdrelais_helper->getName());
        $json_string         = DPDRelaisHelper::getInstance()->getShippingCosts();
        $shippingCostsHelper = new ShippingCostsHelper($json_string, $country_id, $postcode, $weight, $price, $quantity, $quote);
        $method->setPrice($shippingCostsHelper->getPrice());
        $result->append($method);

        /* If total cart weight over 30 kg or 66 lbs, don't display shipping method*/
        $total_weight = $this->getTotalWeight();
        $weightUnit   = $this->_scopeConfig->getValue('general/locale/weight_unit', ScopeInterface::SCOPE_DEFAULT);
        if (($total_weight > 30 && $weightUnit === 'kgs') || ($total_weight > 65 && $weightUnit === 'lbs')) {
            return false;
        }

        return $result;
    }

    /**
     * Get Total Weight of the cart
     * @return float|int
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function getTotalWeight()
    {
        $items        = $this->checkoutSession->getQuote()->getAllItems();
        $total_weight = 0;
        foreach ($items as $item) {
            $total_weight += ($item->getWeight() * $item->getQty());
        }

        return $total_weight;
    }

    /**
     * @return array
     */
    public function getAllowedMethods()
    {
        return ['dpdrelais' => $this->dpdrelais_helper->getName()];
    }
}
