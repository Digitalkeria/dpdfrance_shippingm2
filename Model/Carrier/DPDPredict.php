<?php

namespace DPDFrance\ShippingM2\Model\Carrier;

use DPDFrance\ShippingM2\Helper\Adminhtml\Carrier\DPDPredictHelper;
use DPDFrance\ShippingM2\Helper\Adminhtml\ShippingCostsHelper;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\Model\ResourceModel\Customer\CollectionFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ScopeInterface;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory;
use Magento\Quote\Model\Quote\Address\RateResult\MethodFactory;
use Magento\Shipping\Model\Carrier\AbstractCarrier;
use Magento\Shipping\Model\Carrier\CarrierInterface;
use Magento\Shipping\Model\Rate\ResultFactory;
use Psr\Log\LoggerInterface;

class DPDPredict extends AbstractCarrier implements CarrierInterface
{
    const CODE = 'dpdpredict';
    protected $_code = self::CODE;
    protected $rateResultFactory;
    protected $rateMethodFactory;
    private   $dpdpredict_helper;
    private   $collectionFactory;
    private   $checkoutSession;

    public function __construct(
        CheckoutSession      $checkoutSession,
        ScopeConfigInterface $scopeConfig,
        ErrorFactory         $rateErrorFactory,
        LoggerInterface      $logger,
        ResultFactory        $rateResultFactory,
        MethodFactory        $rateMethodFactory,
        CollectionFactory    $collectionFactory,
        array                $data = []
    )
    {
        $this->rateResultFactory = $rateResultFactory;
        $this->rateMethodFactory = $rateMethodFactory;
        $this->collectionFactory = $collectionFactory;
        $this->dpdpredict_helper = DPDPredictHelper::getInstance();
        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);
        $this->checkoutSession = $checkoutSession;
    }

    /**
     * $request for items and config
     * Shipping price calculation
     * Devra utiliser ShippingCostsHelper pour la gestion des tarifs
     * @param RateRequest $request
     * @return bool|\DPDFrance\ShippingM2\Model\Carrier\Result|\Magento\Shipping\Model\Rate\Result
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function collectRates(RateRequest $request)
    {
        if ($this->dpdpredict_helper->isActive() === false) {
            return false;
        }
        $country_id = $request->getDestCountryId();
        $postcode   = $request->getDestPostcode();
        $weight     = $request->getPackageWeight();
        $price      = $request->getPackageValue();
        $quantity   = $request->getPackageQty();

        $isops = [
            "FR", "MC", "DE", "AT", "CH", "PL", "GB", "ES", "IT", "PT", "CZ",
            "SK", "SI", "NL", "LU", "HU", "LV", "LI", "EE", "BE", "IE", "HR"
        ];

        if (!in_array($country_id, $isops)) {
            return false;
        }

        $quote  = $this->checkoutSession->getQuote();
        $result = $this->rateResultFactory->create();
        $method = $this->rateMethodFactory->create();

        $method->setCarrier('dpdpredict');
        $method->setCarrierTitle($this->dpdpredict_helper->getTitle());
        $method->setMethod('dpdpredict');
        $method->setMethodTitle($this->dpdpredict_helper->getName());
        $json_string = DPDPredictHelper::getInstance()->getShippingCosts();

        //condition pays par la liste de prix
        $prcoid = 0;
        $json   = json_decode($json_string);
        foreach ($json as $entity) {
            if (preg_match("/-\([a-zA-Z0-9,]*\)/", $entity->shipto, $exclude)) {
                $exclude        = $exclude[0];
                $entity->shipto = str_replace($exclude, "", $entity->shipto);
            }
            $countries = explode(",", $entity->shipto);
            foreach ($countries as $pricecountry_id) {
                if ($pricecountry_id === $country_id) {
                    $prcoid = 1;
                    break;
                }
            }
        }

        if ($prcoid === 0) {
            return false;
        }

        $shippingCostsHelper = new ShippingCostsHelper($json_string, $country_id, $postcode, $weight, $price, $quantity, $quote);
        $method->setPrice($shippingCostsHelper->getPrice());
        $result->append($method);

        /* If total cart weight over 30 kg or 66 lbs, don't display shipping method*/
        $total_weight = $this->getTotalWeight();
        $weightUnit   = $this->_scopeConfig->getValue('general/locale/weight_unit', ScopeInterface::SCOPE_DEFAULT);
        if (($total_weight > 30 && $weightUnit === 'kgs') || ($total_weight > 65 && $weightUnit === 'lbs')) {
            return false;
        }

        return $result;
    }

    /**
     * Get Total Weight of the cart
     * @return float|int
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getTotalWeight()
    {
        $items        = $this->checkoutSession->getQuote()->getAllItems();
        $total_weight = 0;
        foreach ($items as $item) {
            $total_weight += ($item->getWeight() * $item->getQty());
        }

        return $total_weight;
    }

    /**
     * @return array
     */
    public function getAllowedMethods()
    {
        return ['dpdpredict' => $this->dpdpredict_helper->getName()];
    }

    public function getDPDPredictHelper()
    {
        return $this->dpdpredict_helper;
    }
}
