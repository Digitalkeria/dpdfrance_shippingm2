<?php

namespace DPDFrance\ShippingM2\Model;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class DPDOption extends AbstractDb
{

    protected function _construct()
    {
        $this->_init("dpd_option", "id");
    }

}
