<?php

namespace DPDFrance\ShippingM2\Model\ResourceModel;

use Magento\Sales\Model\ResourceModel\Order\Grid\Collection;

class ShipmentsCollection extends Collection
{
    /**
     * {@inheritdoc}
     */
    public function _initSelect()
    {
        parent::_initSelect();

        $this->getSelect()
             ->join(
                 $this->getTable("sales_order"),
                 'main_table.entity_id = ' . $this->getTable('sales_order') . '.entity_id and (' . $this->getTable('sales_order') . '.status = "pending" or ' . $this->getTable('sales_order') . '.status = "processing") and ' . $this->getTable('sales_order') . '.shipping_method like "dpd%"',
                 ["increment_id", "created_at", "shipping_method", "weight", "base_total_due", "store_id"]
             )->join(
                 $this->getTable("sales_order_address"),
                 'main_table.entity_id = ' . $this->getTable('sales_order_address') . '.parent_id and ' . $this->getTable('sales_order_address') . '.address_type = "shipping"',
                 ["CONCAT(firstname, ' ', lastname) as customer_name", "company", "street", "postcode", "city", "country_id"]
             )->joinLeft(
                 $this->getTable("dpd_option"),
                 'main_table.entity_id = ' . $this->getTable('dpd_option') . '.order_id',
                 ["advalorem", "retour"]
             )->order("entity_id desc");

        return $this;
    }
}
