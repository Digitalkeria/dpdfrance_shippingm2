<?php
declare(strict_types=1);

namespace DPDFrance\ShippingM2\Model\Service;

use Magento\Framework\App\Request\Http;

class Controller
{
    /**
     * @var Http
     */
    private $request;

    /**
     * @param Http $request
     */
    public function __construct(
        Http $request
    ) {
        $this->request  = $request;
    }

    /**
     * Get is order view
     *
     * @return bool
     */
    public function isOrderView() {

        if ($this->request->getRouteName() == 'sales' && $this->request->getControllerName() == 'order' &&
            $this->request->getActionName() == 'view') {
            return  true;
        }

        return false;
    }
}
