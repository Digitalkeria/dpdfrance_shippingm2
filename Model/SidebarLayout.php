<?php

namespace DPDFrance\ShippingM2\Model;

class SidebarLayout
{
    /**
     * @param array $jsLayout
     * @return array
     */
    public function process($jsLayout)
    {
        $sidebarElements                                                                                           = $jsLayout['components']['checkout']['children']['sidebar']['children']['shipping-information']['children'];
        $jsLayout['components']['checkout']['children']['sidebar']['children']['shipping-information']['children'] = $this->setupSidebar($sidebarElements);

        return $jsLayout;
    }
}
