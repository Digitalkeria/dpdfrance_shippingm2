<?php


namespace DPDFrance\ShippingM2\Plugin\Checkout\Model\Cart;

class UpdateCart
{
    public function afterUpdateItems($subject, $result, $data)
    {
        $address = $result->getQuote()->getShippingAddress();
        $address->collectShippingRates()->save();
        return $result;
    }
}
