<?php

namespace DPDFrance\ShippingM2\Plugin\Checkout\Model;

use DPDFrance\ShippingM2\Helper\Adminhtml\Carrier\DPDRelaisHelper;
use DPDFrance\ShippingM2\Helper\Adminhtml\ShippingCostsHelper;
use Magento\Checkout\Api\Data\ShippingInformationInterface;
use Magento\Checkout\Model\DefaultConfigProvider;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Api\CartRepositoryInterface;

/**
 * Récupération des données saisies dans le formulaire de l'adresse pour le calcul du prix et la vérification des données.
 */
class ShippingInformationManagement
{
    protected $_checkoutSession;
    protected $_quoteRepository;

    /**
     * ShippingInformationManagement constructor.
     * @param CheckoutSession         $checkoutSession
     * @param CartRepositoryInterface $cartRepository
     */
    public function __construct(CheckoutSession $checkoutSession, CartRepositoryInterface $cartRepository)
    {
        $this->_checkoutSession = $checkoutSession;
        $this->_quoteRepository = $cartRepository;
    }

    /**
     * @param \Magento\Checkout\Model\ShippingInformationManagement $subject
     * @param                                                       $cartId
     * @param ShippingInformationInterface                          $addressInformation
     * @return false|void
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function beforeSaveAddressInformation(
        \Magento\Checkout\Model\ShippingInformationManagement $subject,
                                                              $cartId,
        ShippingInformationInterface                          $addressInformation
    )
    {
        $quote = $this->_quoteRepository->getActive($cartId);
        $quote->setRelaisId('');
        $address = $addressInformation->getShippingAddress();
        $address->setCustomerAddressId(0);
        $address->setSaveInAddressBook(0);
        $address->setSameAsBilling(0);


        switch ($addressInformation->getShippingMethodCode()) {
            case "dpdpredict":
                $telephone = $this->_checkoutSession->getData()["steps"]["dpdpredict"]["telephone"];
                $address->setTelephone($telephone);
                $addressInformation->setShippingCarrierCode('dpdpredict');
                break;
            case "dpdrelais":
                $relais_id = $this->_checkoutSession->getData()["steps"]["dpdrelais"]["relais_id"];
                $name      = $this->_checkoutSession->getData()["steps"]["dpdrelais"]["name"];
                $street    = $this->_checkoutSession->getData()["steps"]["dpdrelais"]["street"];
                $postcode  = $this->_checkoutSession->getData()["steps"]["dpdrelais"]["postcode"];
                $city      = $this->_checkoutSession->getData()["steps"]["dpdrelais"]["city"];
                $address->setCompany($name . " " . $relais_id);
                $address->setStreet($street);
                $address->setPostcode($postcode);
                $address->setCity($city);
                $address->setCountryId("FR");

                $addressInformation->setShippingAddress($address);
                $addressInformation->setShippingCarrierCode('dpdrelais');

                $quote->setShippingAddress($address)->setRelaisId($relais_id);

                $json_string = DPDRelaisHelper::getInstance()->getShippingCosts();
                $weight      = (float)$this->_checkoutSession->getQuote()->getItemsQty();
                $price       = (float)$this->_checkoutSession->getQuote()->getSubtotal();
                $quantity    = (int)$this->_checkoutSession->getQuote()->getItemsQty();
                $helper      = new ShippingCostsHelper($json_string, "FR", $postcode, $weight, $price, $quantity, $quote);
                $price       = $helper->getPrice();
                $this->_checkoutSession->getQuote()->getShippingAddress()->setShippingAmount($price);
                break;
            default:
                break;
        }

    }

    /**
     *
     * @param DefaultConfigProvider $config
     * @param                       $output
     * @return array
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function afterGetConfig(DefaultConfigProvider $config, $output)
    {
        return $this->getCustomQuoteData($output);
    }

    /**
     * @param $output
     * @return array
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    private function getCustomQuoteData($output): array
    {
        if ($this->_checkoutSession->getQuote()->getId()) {
            $quote                                         = $this->_quoteRepository->get($this->_checkoutSession->getQuote()->getId());
            $output['quoteData']['quote_shipping_address'] = $quote->getShippingAddress()->getData()['customer_address_id'];
            $output['quoteData']['quote_billing_address']  = $quote->getBillingAddress()->getData()['customer_address_id'];
            $output['quoteData']['quote_paymentmethod']    = $quote->getPayment()->getMethod();
        }
        return $output;
    }

}
