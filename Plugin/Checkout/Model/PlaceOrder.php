<?php

namespace DPDFrance\ShippingM2\Plugin\Checkout\Model;

use DPDFrance\ShippingM2\Helper\Adminhtml\SetupHelper;
use DPDFrance\ShippingM2\Helper\Adminhtml\ShipmentsHelper;
use Magento\Sales\Api\Data\OrderInterface;

class PlaceOrder
{
    protected $_setupHelper;
    protected $_shipmentsHelper;

    public function __construct()
    {
        $this->_setupHelper     = SetupHelper::getInstance();
        $this->_shipmentsHelper = ShipmentsHelper::getInstance();
    }

    /**
     * @param \Magento\Sales\Api\Data\OrderInterface
     */
    public function beforePlace(OrderInterface $order)
    {
        $order_id  = $order->getIncrementId();
        $advalorem = $this->_setupHelper->getAssurance();
        if ($advalorem == "") {
            $advalorem = 0;
        }
        $retour = $this->_setupHelper->getRetour();
        $this->_shipmentsHelper->insertInDPDOption($order_id, $advalorem, $retour);
    }
}
