<?php

/*
 * Enregistrement du module en interne dans Magento
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'DPDFrance_ShippingM2',
    __DIR__
);