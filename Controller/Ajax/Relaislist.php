<?php

namespace DPDFrance\ShippingM2\Controller\Ajax;

use DPDFrance\ShippingM2\Helper\Adminhtml\Carrier\DPDRelaisHelper;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\LayoutFactory;
use SimpleXMLElement;
use SoapClient;
use SoapFault;

class Relaislist extends Action
{
    /**
     * @var JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var LayoutFactory
     */
    protected $layoutFactory;

    /**
     * @var HelperData
     */
    protected $relaisHelper;

    /**
     * @var Template
     */
    protected $block;

    /**
     * getRelais constructor.
     * @param Context $context
     * @param JsonFactory $jsonFactory
     * @param LayoutFactory $layoutFactory
     * @param DPDRelaisHelper $relaisHelper
     */
    public function __construct(Context $context, JsonFactory $jsonFactory, LayoutFactory $layoutFactory, DPDRelaisHelper $relaisHelper)
    {
        parent::__construct($context);
        $this->resultJsonFactory = $jsonFactory;
        $this->layoutFactory     = $layoutFactory;
        $this->relaisHelper      = $relaisHelper;
        $object_manager          = ObjectManager::getInstance();
        $this->block             = $object_manager->get(Template::class);
    }

    /**
     * Action de la requête Ajax relaisList
     * @return Json
     * @throws SoapFault
     */
    public function execute()
    {
        $address = $this->getRequest()->getParam('address');
        $address = mb_convert_encoding(urldecode($address), 'UTF-8');
        $address = self::stripAccents($address);
        $zipcode = $this->getRequest()->getParam('zipcode');
        $zipcode = trim(urldecode($zipcode));
        $zipcode = mb_convert_encoding($zipcode, 'UTF-8');

        if ($zipcode === "") {
            $resultData            = [];
            $result                = $this->resultJsonFactory->create();
            $html                  = '<ul class="messages"><li class="warnmsg"><ul><li>' . __('The field Postal Code is mandatory!') . '</li></ul></li></ul>';
            $resultData['content'] = $html;
            $result->setData($resultData);
            return $result;
        }

        $city       = $this->getRequest()->getParam('city');
        $city       = mb_convert_encoding(urldecode($city), 'UTF-8');
        $city       = self::stripAccents($city);
        $variables  = [
            'carrier'             => $this->relaisHelper->getIdMarchand(),
            'key'                 => $this->relaisHelper->getSecurityKey(),
            'address'             => $address,
            'zipCode'             => $zipcode,
            'city'                => $city,
            'countrycode'         => 'FR',
            'requestID'           => '1234',
            'request_id'          => '1234',
            'date_from'           => date('d/m/Y'),
            'max_pudo_number'     => '', //mettre 5
            'max_distance_search' => '',
            'weight'              => '',
            'category'            => '',
            'holiday_tolerant'    => ''
        ];
        $result     = $this->resultJsonFactory->create();
        $resultData = [];
        $addresses  = [];
        $html       = "";

        if (!extension_loaded('soap')) {
            $html                  = '<ul class="messages"><li class="warnmsg"><ul><li>' . __('ATTENTION! L\'extension PHP SOAP n\'est pas activée sur ce serveur. Vous devez l\'activer pour utiliser le module DPD Relais.') . '</li></ul></li></ul>';
            $resultData['content'] = $html;
            $result->setData($resultData);
            return $result;
        }

        $serviceUrl = $this->relaisHelper->getUrlWebservice();

        try {
            ini_set("default_socket_timeout", 3);
            $soappudo = new SoapClient($serviceUrl, [
                'connection_timeout' => 3,
                'cache_wsdl'         => WSDL_CACHE_NONE,
                'exceptions'         => true
            ]);

            // * Appel SOAP a l'applicatif GetPudoList
            $GetPudoList = $soappudo->getPudoList($variables);

        } catch (Exception $e) {
            $html                  .= '<ul class="messages"><li class="warnmsg"><ul><li>' . __('An error ocurred while fetching the DPD Pickup points. Please try again') . '</li></ul></li></ul>';
            $resultData['content'] = $html;
            $result->setData($resultData);
            return $result;
        }

        $doc_xml = new SimpleXMLElement($GetPudoList->GetPudoListResult->any);
        $quality = (int)$doc_xml->attributes()->quality; // indice de qualité de la réponse SOAP

        if ($doc_xml->xpath('ERROR')) { // si le webservice répond un code erreur, afficher un message d'indisponibilité
            $html = '<ul class="messages"><li class="warnmsg"><ul><li>' . __('An error ocurred while fetching the DPD Pickup points. Please try again') . '</li></ul></li></ul>';
        } else {
            if ((int)$quality === 0) {
                // * Si la qualité de la réponse est 0, "merci d'indiquer une autre adresse"
                $html = '<ul class="messages"><li class="warnmsg"><ul><li>' . __('There are no DPD Pickup points for the selected address. Please modify it.') . '</li></ul></li></ul>';
            } else {
                // * Accéder a la balise pudo_items
                $allpudoitems = $doc_xml->xpath('PUDO_ITEMS');

                foreach ($allpudoitems as $singlepudoitem) {
                    // * Éclatement des données contenues dans pudo_items
                    $oneResult = $singlepudoitem->xpath('PUDO_ITEM');
                    $i         = 0;
                    foreach ($oneResult as $result2) {
                        $relais_id             = (string)$result2->PUDO_ID;
                        $mapPositions[]        = [
                            'latitude'  => (float)str_replace(",", ".", (string)$result2->LATITUDE),
                            'longitude' => (float)str_replace(",", ".", (string)$result2->LONGITUDE),
                        ];
                        $addresses[$relais_id] = [
                            'street'   => (string)$result2->ADDRESS1,
                            'postcode' => (string)$result2->ZIPCODE,
                            'city'     => (string)$result2->CITY,
                            'name'     => (string)$result2->NAME
                        ];
                        $html                  .= '
                                <div>
                                    <span class="dpdfrrelais_logo"><img src="' . $this->block->getViewFileUrl('DPDFrance_ShippingM2::images/relais/pointrelais.png') . '" alt="-"/></span>
                                    <span class="s1"><strong>' . self::stripAccents($result2->NAME) . '</strong><br/>' . self::stripAccents($result2->ADDRESS1) . ' <br/> ' . $result2->ZIPCODE . ' ' . self::stripAccents($result2->CITY) . '</span>
                                    <span class="s2">' . sprintf("%01.2f", (int)$result2->DISTANCE / 1000) . ' km  </span>
                                    <span class="s3"><a id="moredetails' . $i . '" style="cursor:pointer;">' . __('More details') . '</a></span>
                                    <input type="radio" id="relay-point' . $i . '" name="relay-point" class="dpdfrrelais_radio" value="' . self::stripAccents($result2->ADDRESS1) . '  ' . self::stripAccents($result2->ADDRESS2) . '|||' . self::stripAccents($result2->NAME) . '  ' . (string)$result2->PUDO_ID . '|||' . $result2->ZIPCODE . '|||' . self::stripAccents($result2->CITY) . '">
                                    <label id="' . $relais_id . '" class="dpdfrrelais_button_ici" for="relay-point' . $i . '"><span><span></span></span><b>ICI</b></label>
                                </div>
                                ';

                        $days  = [1 => 'monday', 2 => 'tuesday', 3 => 'wednesday', 4 => 'thursday', 5 => 'friday', 6 => 'saturday', 7 => 'sunday'];
                        $point = [];
                        $item  = (array)$result2;

                        if (count($item['OPENING_HOURS_ITEMS']->OPENING_HOURS_ITEM) > 0) {
                            foreach ($item['OPENING_HOURS_ITEMS']->OPENING_HOURS_ITEM as $k => $oh_item) {
                                $oh_item                            = (array)$oh_item;
                                $point[$days[$oh_item['DAY_ID']]][] = $oh_item['START_TM'] . ' - ' . $oh_item['END_TM'];
                            }
                        }

                        if (empty($point['monday'])) {
                            $h1 = __('Closed');
                        } else {
                            $h1 = empty($point['monday'][1]) ?
                                $point['monday'][0] :
                                $point['monday'][0] . ' & ' . $point['monday'][1];
                        }

                        if (empty($point['tuesday'])) {
                            $h2 = __('Closed');
                        } else {
                            $h2 = empty($point['tuesday'][1]) ?
                                $point['tuesday'][0] :
                                $point['tuesday'][0] . ' & ' . $point['tuesday'][1];
                        }

                        if (empty($point['wednesday'])) {
                            $h3 = __('Closed');
                        } else {
                            $h3 = empty($point['wednesday'][1]) ?
                                $point['wednesday'][0] :
                                $point['wednesday'][0] . ' & ' . $point['wednesday'][1];
                        }

                        if (empty($point['thursday'])) {
                            $h4 = __('Closed');
                        } else {
                            $h4 = empty($point['thursday'][1]) ?
                                $point['thursday'][0] :
                                $point['thursday'][0] . ' & ' . $point['thursday'][1];
                        }

                        if (empty($point['friday'])) {
                            $h5 = __('Closed');
                        } else {
                            $h5 = empty($point['friday'][1]) ?
                                $point['friday'][0] :
                                $point['friday'][0] . ' & ' . $point['friday'][1];
                        }

                        if (empty($point['saturday'])) {
                            $h6 = __('Closed');
                        } else {
                            $h6 = empty($point['saturday'][1]) ?
                                $point['saturday'][0] :
                                $point['saturday'][0] . ' & ' . $point['saturday'][1];
                        }

                        if (empty($point['sunday'])) {
                            $h7 = __('Closed');
                        } else {
                            $h7 = empty($point['sunday'][1]) ?
                                $point['sunday'][0] :
                                $point['sunday'][0] . ' & ' . $point['sunday'][1];
                        }

                        $html .= '<div id="relaydetail' . $i . '" style="display:none; background-color: white;">
                                            <div class="dpdfrrelaisboxcarto" id="map_canvas' . $i . '" style="width:100%; height: 500px;"></div>
                                            <div id="dpdfrrelaisboxbottom" class="dpdfrrelaisboxbottom">
                                                <div id="dpdfrrelaisboxadresse" class="dpdfrrelaisboxadresse">
                                                    <div class="dpdfrrelaisboxadresseheader"><img src="' . $this->block->getViewFileUrl('DPDFrance_ShippingM2::images/relais/pointrelais.png') . '" alt="-" width="32" height="32"/><br/>' . __('Your DPD Pickup point') . '</div>
                                                    <strong>' . $result2->NAME . '</strong></br>
                                                    ' . $result2->ADDRESS1 . '</br>';


                        if (!empty($result2->ADDRESS2)) {
                            $html .= $result2->ADDRESS2 . '</br>';
                        }
                        $html .= $result2->ZIPCODE . '  ' . $result2->CITY . '<br/>';
                        if (!empty($result2->LOCAL_HINT)) {
                            $html .= '<p>' . __('info') . '  :  ' . $result2->LOCAL_HINT . '</p>';
                        }
                        $html .= '</div>';
                        $html .= '<div class="dpdfrrelaisboxhoraires">
                                    <div class="dpdfrrelaisboxhorairesheader">
                                        <img src="' . $this->block->getViewFileUrl('DPDFrance_ShippingM2::images/relais/horaires.png') . '" alt="-" width="32" height="32"/><br/>' . __('Opening hours') .
                                 '</div>
                                    <p><span>' . __('Monday') . ' : </span>' . $h1 . '</p>
                                    <p><span>' . __('Tuesday') . ' : </span>' . $h2 . '</p>
                                    <p><span>' . __('Wednesday') . ' : </span>' . $h3 . '</p>
                                    <p><span>' . __('Thursday') . ' : </span>' . $h4 . '</p>
                                    <p><span>' . __('Friday') . ' : </span>' . $h5 . '</p>
                                    <p><span>' . __('Saturday') . ' : </span>' . $h6 . '</p>
                                    <p><span>' . __('Sunday') . ' : </span>' . $h7 . '</p>
                                  </div>';

                        $html .= '<div class="dpdfrrelaisboxinfos">
                                    <div class="dpdfrrelaisboxinfosheader">
                                        <img src="' . $this->block->getViewFileUrl('DPDFrance_ShippingM2::images/relais/info.png') . '" alt="-" width="32" height="32"/><br/>' . __('More info') .
                                 '</div>
                                    <div><h5>' . __('Distance in KM') . '  :  </h5><strong>' . sprintf("%01.2f", $result2->DISTANCE / 1000) . ' km </strong></div>
                                    <div><h5>' . __('DPD Pickup ID#') . '  :  </h5><strong>' . (string)$result2->PUDO_ID . '</strong></div>';
                        if (count($result2->HOLIDAY_ITEMS->HOLIDAY_ITEM) > 0) {
                            foreach ($result2->HOLIDAY_ITEMS->HOLIDAY_ITEM as $holiday_item) {
                                $holiday_item = (array)$holiday_item;
                                $html         .= '<div><img id="dpdfrrelaisboxinfoswarning" src="' . $this->block->getViewFileUrl('DPDFrance_ShippingM2::images/relais/pointrelais.png') . '" alt="-" width="16" height="16"/> <h4>' . __('Closing period') . '  : </h4> ' . $holiday_item['START_DTM'] . ' - ' . $holiday_item['END_DTM'] . '</div>';
                            }
                        }
                        // * dpdfrrelaisboxbottom et relaydetail
                        $html .= '</div></div>';
                        $html .= "</div>";

                        $i++;
                        $hd1 = $hd2 = $hd3 = $hd4 = $hd5 = $hd6 = $hd7 = $h1 = $h2 = $h3 = $h4 = $h5 = $h6 = $h7 = null;
                        // * Nombre de points relais à afficher - limite a 5
                        if ($i === 5) {
                            break;
                        }
                    }
                }
            }
        }

        $resultData['content']     = $html;
        $resultData['relaisCount'] = count($mapPositions);
        $resultData['js']          = $this->getInitMap($mapPositions);
        $resultData['addresses']   = $addresses;
        $result->setData($resultData);
        return $result;
    }

    /**
     * On envoie l'affichage du frontend de la liste des relais en réponse du controller, on envoie aussi la carte des relais
     * @param $str
     * @return array|string|string[]|null
     */
    public static function stripAccents($str)
    {
        $str = preg_replace('/[\x{00C0}\x{00C1}\x{00C2}\x{00C3}\x{00C4}\x{00C5}]/u', 'A', $str);
        $str = preg_replace('/[\x{0105}\x{0104}\x{00E0}\x{00E1}\x{00E2}\x{00E3}\x{00E4}\x{00E5}]/u', 'a', $str);
        $str = preg_replace('/[\x{00C7}\x{0106}\x{0108}\x{010A}\x{010C}]/u', 'C', $str);
        $str = preg_replace('/[\x{00E7}\x{0107}\x{0109}\x{010B}\x{010D}}]/u', 'c', $str);
        $str = preg_replace('/[\x{010E}\x{0110}]/u', 'D', $str);
        $str = preg_replace('/[\x{010F}\x{0111}]/u', 'd', $str);
        $str = preg_replace('/[\x{00C8}\x{00C9}\x{00CA}\x{00CB}\x{0112}\x{0114}\x{0116}\x{0118}\x{011A}]/u', 'E', $str);
        $str = preg_replace('/[\x{00E8}\x{00E9}\x{00EA}\x{00EB}\x{0113}\x{0115}\x{0117}\x{0119}\x{011B}]/u', 'e', $str);
        $str = preg_replace('/[\x{00CC}\x{00CD}\x{00CE}\x{00CF}\x{0128}\x{012A}\x{012C}\x{012E}\x{0130}]/u', 'I', $str);
        $str = preg_replace('/[\x{00EC}\x{00ED}\x{00EE}\x{00EF}\x{0129}\x{012B}\x{012D}\x{012F}\x{0131}]/u', 'i', $str);
        $str = preg_replace('/[\x{0142}\x{0141}\x{013E}\x{013A}]/u', 'l', $str);
        $str = preg_replace('/[\x{00F1}\x{0148}]/u', 'n', $str);
        $str = preg_replace('/[\x{00D2}\x{00D3}\x{00D4}\x{00D5}\x{00D6}\x{00D8}]/u', 'O', $str);
        $str = preg_replace('/[\x{00F2}\x{00F3}\x{00F4}\x{00F5}\x{00F6}\x{00F8}]/u', 'o', $str);
        $str = preg_replace('/[\x{0159}\x{0155}]/u', 'r', $str);
        $str = preg_replace('/[\x{015B}\x{015A}\x{0161}]/u', 's', $str);
        $str = preg_replace('/[\x{00DF}]/u', 'ss', $str);
        $str = preg_replace('/[\x{0165}]/u', 't', $str);
        $str = preg_replace('/[\x{00D9}\x{00DA}\x{00DB}\x{00DC}\x{016E}\x{0170}\x{0172}]/u', 'U', $str);
        $str = preg_replace('/[\x{00F9}\x{00FA}\x{00FB}\x{00FC}\x{016F}\x{0171}\x{0173}]/u', 'u', $str);
        $str = preg_replace('/[\x{00FD}\x{00FF}]/u', 'y', $str);
        $str = preg_replace('/[\x{017C}\x{017A}\x{017B}\x{0179}\x{017E}]/u', 'z', $str);
        $str = preg_replace('/[\x{00C6}]/u', 'AE', $str);
        $str = preg_replace('/[\x{00E6}]/u', 'ae', $str);
        $str = preg_replace('/[\x{0152}]/u', 'OE', $str);
        $str = preg_replace('/[\x{0153}]/u', 'oe', $str);
        $str = preg_replace('/[\x{0022}\x{0025}\x{0026}\x{0027}\x{00A1}\x{00A2}\x{00A3}\x{00A4}\x{00A5}\x{00A6}\x{00A7}\x{00A8}\x{00AA}\x{00AB}\x{00AC}\x{00AD}\x{00AE}\x{00AF}\x{00B0}\x{00B1}\x{00B2}\x{00B3}\x{00B4}\x{00B5}\x{00B6}\x{00B7}\x{00B8}\x{00BA}\x{00BB}\x{00BC}\x{00BD}\x{00BE}\x{00BF}]/u', ' ', $str);
        return $str;
    }

    /**
     * Initialise Google Map
     * @param $mapPositions
     * @return string
     */
    function getInitMap($mapPositions)
    {
        $styles     = 'styles: [{"featureType":"landscape","stylers":[{"visibility":"on"},{"color":"#ebebeb"}]},{"featureType":"poi.sports_complex","stylers":[{"visibility":"on"}]},{"featureType":"poi.attraction","stylers":[{"visibility":"off"}]},{"featureType":"poi.government","stylers":[{"visibility":"on"}]},{"featureType":"poi.medical","stylers":[{"visibility":"on"}]},{"featureType":"poi.place_of_worship","stylers":[{"visibility":"on"}]},{"featureType":"poi.school","stylers":[{"visibility":"on"}]},{"featureType":"water","elementType":"geometry","stylers":[{"visibility":"on"},{"color":"#d2e4f3"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"on"}]},{"featureType":"transit","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#ffffff"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"on"},{"color":"#ebebeb"}]},{"elementType":"labels.text.fill","stylers":[{"visibility":"on"},{"color":"#666666"}]},{"featureType":"poi.business","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#dbdbdb"}]},{"featureType":"administrative.locality","elementType":"labels.text.fill","stylers":[{"visibility":"on"},{"color":"#999999"}]},{"featureType":"transit.station","stylers":[{"visibility":"on"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"visibility":"on"},{"color":"#dbdbdb"}]},{"elementType":"labels.icon","stylers":[{"visibility":"on"},{"saturation":-100}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"elementType":"labels.text","stylers":[{"visibility":"on"}]},{"featureType":"transit.line","elementType":"labels.text","stylers":[{"visibility":"off"}]}]';
        $markerIcon = $this->block->getViewFileUrl('DPDFrance_ShippingM2::images/relais/logo-max-png.png');
        $js         = '<script type="application/javascript">
                         function initMap(){
                            let map; let marker;
                      ';
        $i          = 0;

        foreach ($mapPositions as $mapPosition) {
            $center   = 'center: {lat: ' . $mapPosition['latitude'] . ', lng: ' . $mapPosition['longitude'] . '}';
            $position = 'position: {lat: ' . $mapPosition['latitude'] . ', lng: ' . $mapPosition['longitude'] . '}';
            $icon     = 'icon : "' . $markerIcon . '"';
            $js       .= 'map = new google.maps.Map(document.getElementById("map_canvas' . $i . '"), { zoom: 15, ' . $center . ', mapTypeId: google.maps.MapTypeId.ROADMAP, ' . $styles . '});';
            $js       .= 'marker = new google.maps.Marker({' . $position . ', map: map, animation : google.maps.Animation.DROP, ' . $icon . '});';
            $i++;
        }
        $js .= "}</script>";
        $js .= '<script src="https://maps.googleapis.com/maps/api/js?key=' . $this->relaisHelper->getGoogleMapsApiKey() . '&callback=initMap" async defer></script>';
        return $js;
    }
}

?>
