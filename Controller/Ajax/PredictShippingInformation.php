<?php

namespace DPDFrance\ShippingM2\Controller\Ajax;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\Controller\ResultInterface;

class PredictShippingInformation extends Action
{
    /**
     * @var JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var CheckoutSession
     */
    protected $checkoutSession;

    /**
     * @param Context         $context
     * @param JsonFactory     $jsonFactory
     * @param CheckoutSession $checkoutSession
     */
    public function __construct(Context $context, JsonFactory $jsonFactory, CheckoutSession $checkoutSession)
    {
        parent::__construct($context);
        $this->resultJsonFactory = $jsonFactory;
        $this->checkoutSession   = $checkoutSession;
    }

    /**
     * Récupération du numéro de téléphone pour vérifier la compatibilité Predict.
     * @return ResponseInterface|Json|ResultInterface
     */
    public function execute()
    {
        $telephone = $this->getRequest()->getParam('telephone');
        $this->checkoutSession->setStepData('dpdpredict', 'telephone', $telephone);
        $result             = $this->resultJsonFactory->create();
        $resultData         = [];
        $resultData['data'] = $this->checkoutSession->getData();
        $result->setData($resultData);
        return $result;
    }
}
