<?php

namespace DPDFrance\ShippingM2\Controller\Ajax;

use DPDFrance\ShippingM2\Helper\Adminhtml\Carrier\DPDClassicHelper;
use DPDFrance\ShippingM2\Helper\Adminhtml\ShippingCostsHelper;
use Magento\Framework\App\Action\Action;

class GestionTarifs extends Action
{
    public function execute()
    {
        $json_string = DPDClassicHelper::getInstance()->getShippingCosts();
        $country     = "FR";
        $postcode    = "20400";
        $weight      = 20;
        $price       = 92;
        $quantity    = 1;
        $helper      = new ShippingCostsHelper($json_string, $country, $postcode, $weight, $price, $quantity);
        $helper->toString();
    }
}
