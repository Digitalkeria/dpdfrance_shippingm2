<?php

namespace DPDFrance\ShippingM2\Controller\Ajax;

use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\Api\AddressRepositoryInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Controller\Result\JsonFactory;
use Psr\Log\LoggerInterface;

class RelaisShippingInformation extends Action
{
    /**
     * @var JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var CheckoutSession
     */
    protected $checkoutSession;

    /**
     * @param Context $context
     * @param JsonFactory $jsonFactory
     * @param CheckoutSession $checkoutSession
     */
    public function __construct(Context $context, JsonFactory $jsonFactory, CheckoutSession $checkoutSession)
    {
        parent::__construct($context);
        $this->resultJsonFactory = $jsonFactory;
        $this->checkoutSession   = $checkoutSession;
    }

    public function execute()
    {
        $relais_id = $this->getRequest()->getParam('relais_id');
        $name      = $this->getRequest()->getParam('name');
        $street    = $this->getRequest()->getParam('street');
        $postcode  = $this->getRequest()->getParam('postcode');
        $city      = $this->getRequest()->getParam('city');

        $this->checkoutSession->setStepData('dpdrelais', 'relais_id', $relais_id);
        $this->checkoutSession->setStepData('dpdrelais', 'name', $name);
        $this->checkoutSession->setStepData('dpdrelais', 'street', $street);
        $this->checkoutSession->setStepData('dpdrelais', 'postcode', $postcode);
        $this->checkoutSession->setStepData('dpdrelais', 'city', $city);

        $result             = $this->resultJsonFactory->create();
        $resultData         = [];
        $resultData['data'] = $this->checkoutSession->getData();
        $result->setData($resultData);
        return $result;
    }
}
