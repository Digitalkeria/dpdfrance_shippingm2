<?php

namespace DPDFrance\ShippingM2\Controller\Ajax;

use DPDFrance\ShippingM2\Helper\Adminhtml\SetupHelper;
use DPDFrance\ShippingM2\Helper\Adminhtml\ShipmentsHelper;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\View\LayoutFactory;

class AdvaloremRetour extends Action
{

    /**
     * @var JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var LayoutFactory
     */
    protected $layoutFactory;

    /**
     * @var HelperData
     */
    protected $predictHelper;

    /**
     * @var ShipmentsHelper
     */
    protected $shipmentsHelper;

    /**
     * getRelais constructor.
     * @param Context $context
     * @param JsonFactory $jsonFactory
     * @param LayoutFactory $layoutFactory
     */
    public function __construct(
        Context       $context,
        JsonFactory   $jsonFactory,
        LayoutFactory $layoutFactory)
    {
        parent::__construct($context);
        $this->resultJsonFactory = $jsonFactory;
        $this->layoutFactory     = $layoutFactory;
        $this->shipmentsHelper   = ShipmentsHelper::getInstance();
    }

    /**
     * On envoie l'affichage du frontend du predict en réponse du controller
     * Mise à jour dans la table DPDOption pour l'assurance et du retour
     * @return Json
     */
    public function execute()
    {
        $order_id = $this->getRequest()->getParam("order_id");
        $option   = $this->getRequest()->getParam("option");
        $checked  = $this->getRequest()->getParam("checked");

        switch ($option) {
            case "advalorem":
                $checked = $checked !== "false" ? "1" : "0";
                $this->shipmentsHelper->updateInDPDOptionAdvalorem($order_id, $checked);
                break;
            case "retour":
                $checked = $checked !== "false" ?
                    SetupHelper::getInstance()->getRetour() :
                    "0";
                $this->shipmentsHelper->updateInDPDOptionRetour($order_id, $checked);
                break;
        }
    }
}
