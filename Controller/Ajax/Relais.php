<?php

namespace DPDFrance\ShippingM2\Controller\Ajax;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\View\LayoutFactory;
use Magento\Framework\View\Element\Template;

class Relais extends Action
{
    /**
     * @var JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var LayoutFactory
     */
    protected $layoutFactory;

    /**
     * @var HelperData
     */
    protected $relaisHelper;

    /**
     * getRelais constructor.
     * @param Context $context
     * @param JsonFactory $jsonFactory
     * @param LayoutFactory $layoutFactory
     */
    public function __construct(Context $context, JsonFactory $jsonFactory, LayoutFactory $layoutFactory)
    {
        parent::__construct($context);
        $this->resultJsonFactory = $jsonFactory;
        $this->layoutFactory     = $layoutFactory;
    }

    /**
     * On envoie l'affichage du frontend du relais en réponse du controller
     * @return Json
     */
    public function execute()
    {
        $resultData            = [];
        $result                = $this->resultJsonFactory->create();
        $layout                = $this->layoutFactory->create();
        $content               = $layout->createBlock(Template::class)->setTemplate("DPDFrance_ShippingM2::relais.phtml")->toHtml();
        $resultData['content'] = $content;
        $result->setData($resultData);
        return $result;
    }

}
