<?php
//Fichier pour passer le statut en processing et créer l'expédition
namespace DPDFrance\ShippingM2\Controller\Adminhtml\Shipments;

use DPDFrance\ShippingM2\Helper\Adminhtml\Carrier\DPDClassicHelper;
use DPDFrance\ShippingM2\Helper\Adminhtml\Carrier\DPDPredictHelper;
use DPDFrance\ShippingM2\Helper\Adminhtml\Carrier\DPDRelaisHelper;
use DPDFrance\ShippingM2\Helper\Adminhtml\ShipmentsHelper;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\ObjectManager;
use Magento\Sales\Api\Data\ShipmentCommentCreationInterface;
use Magento\Sales\Model\Order\Email\Sender\ShipmentCommentSender;
use Magento\Sales\Model\Order\Shipment\TrackFactory;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Sales\Model\Convert\Order;

class ProcessOrder extends Action
{
    protected $_collectionFactory;
    protected $_filter;
    protected $_shipmentsHelper;
    protected $_trackingFactory;
    protected $_commentInterface;
    protected $_shipmentCommentSender;

    public function __construct(
        Context                          $context,
        CollectionFactory                $collectionFactory,
        Filter                           $filter,
        ShipmentsHelper                  $shipmentsHelper,
        TrackFactory                     $trackFactory,
        ShipmentCommentCreationInterface $commentInterface,
        ShipmentCommentSender            $shipmentCommentSender
    )
    {
        parent::__construct($context);

        $this->_collectionFactory = $collectionFactory;
        $this->_filter            = $filter;
        $this->_shipmentsHelper   = $shipmentsHelper;
        // * instance of tracking factory to add tracking number in shipment
        $this->_trackingFactory       = $trackFactory;
        $this->_commentInterface      = $commentInterface;
        $this->_shipmentCommentSender = $shipmentCommentSender;
    }

    public function execute()
    {
        $collection    = $this->_filter->getCollection($this->_collectionFactory->create());
        $objectManager = ObjectManager::getInstance();
        $error         = false;

        foreach ($collection->getItems() as $order) {
            $order_inc_id = $order->getIncrementId();
            $order_id     = $order->getId();
            $email        = $order->getCustomerEmail();

            // * Envoi EMAIL avec lien de suivi de colis
            switch ($order["shipping_method"]) {
                case "dpdclassic_dpdclassic":
                    $customer_ag = DPDClassicHelper::getInstance()->getAgenceDPD();
                    $customer_id = DPDClassicHelper::getInstance()->getContract();
                    $carrierCode = 'dpdclassic';
                    break;
                case "dpdpredict_dpdpredict":
                    $customer_ag = DPDPredictHelper::getInstance()->getAgenceDPD();
                    $customer_id = DPDPredictHelper::getInstance()->getContract();
                    $carrierCode = 'dpdpredict';
                    break;
                case "dpdrelais_dpdrelais":
                    $customer_ag = DPDRelaisHelper::getInstance()->getAgenceDPD();
                    $customer_id = DPDRelaisHelper::getInstance()->getContract();
                    $carrierCode = 'dpdrelais';
                    break;
            }

            $trackingNumber = $order_inc_id . "_" . $customer_ag . $customer_id;
            $url            = "https://www.dpd.fr/tracex_" . $order_inc_id . "_" . $customer_ag . $customer_id;
            $trackingTitle  = 'DPD France';
            $notify         = true;
            $commentTrack   = 'Cher client, vous pouvez suivre l\'acheminement de votre colis par DPD France en cliquant sur le lien ci-contre : "' . $url . '"';

            // * Vérifie si possible de créer une expédition selon ORDER
            if (!$order->canShip()) {
                $message = __("Expédition " . $order_inc_id . " déjà créé, merci d'en sélectionner une autre.");
                $this->messageManager->addError($message);
                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath('dpdfrance/shipments/index');
                return $resultRedirect;
            }

            // * Initialiser le shipment
            $convertOrder = $objectManager->create(Order::class);
            $shipment     = $convertOrder->toShipment($order);

            // * Produits sans virtual
            foreach ($order->getAllItems() as $orderItem) {
                if (!$orderItem->getQtyToShip()) {
                    continue;
                }
                if ($orderItem->getIsVirtual()) {
                    continue;
                }

                $item = $convertOrder->itemToShipmentItem($orderItem);
                $qty  = $orderItem->getQtyToShip();
                $item->setQty($qty);
                $shipment->addItem($item);
            }

            // * Sauvegarder une expédition (register)
            $shipment->register();
            // * ADD Track
            $shipment->addTrack($this->_trackingFactory->create()->setTrackNumber($url)->setCarrierCode($carrierCode)->setTitle($trackingTitle)->setUrl($url));
            // * Order Processing
            $shipment->getOrder()->setIsInProcess(true);
            // * SetEmailSent(true)
            if ($email != null) {
                $shipment->setEmailSent(true);
            }
            // * try SendEmail();
            try {
                // * Email sending
                $shipment->save();
                $shipment->getOrder()->save();
                $this->_shipmentCommentSender->send($shipment, $notify, $commentTrack);
                $shipment->save();
            } catch (Exception $e) {
                $message = __('Erreur pendant la création de l\'expédition %s : %s', $order_id, $e->getMessage());
                $this->messageManager->addError($message);
                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath('dpdfrance/shipments/index');
                return $resultRedirect;
            }

        }

        $message = __("Le statut de/des expédition(s) a été passé en expédiée/processing");
        $this->messageManager->addSuccess($message);
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath("dpdfrance/shipments/index");
        return $resultRedirect;
    }

}
