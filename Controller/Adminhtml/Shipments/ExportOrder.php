<?php

namespace DPDFrance\ShippingM2\Controller\Adminhtml\Shipments;

use DPDFrance\ShippingM2\Helper\Adminhtml\Carrier\DPDClassicHelper;
use DPDFrance\ShippingM2\Helper\Adminhtml\Carrier\DPDPredictHelper;
use DPDFrance\ShippingM2\Helper\Adminhtml\Carrier\DPDRelaisHelper;
use DPDFrance\ShippingM2\Helper\Adminhtml\Carrier\PredictHelper;
use DPDFrance\ShippingM2\Helper\Adminhtml\SetupHelper;
use DPDFrance\ShippingM2\Helper\Adminhtml\ShipmentsHelper;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\View\Result\PageFactory;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Magento\Ui\Component\MassAction\Filter;

class ExportOrder extends Action
{
    /**
     * @var Magento\Sales\Model\ResourceModel\Order\CollectionFactory
     */
    protected $_collectionFactory;

    /**
     * @var FileFactory
     */
    protected $_fileFactory;

    /**
     * @var Magento\Ui\Component\MassAction\Filter
     */
    protected $_filter;

    /**
     * @var Order
     */
    protected $order;

    /**
     * @var SetupHelper
     */
    protected $_setupHelper;

    /**
     * @var DPDClassicHelper
     */
    protected $_classicHelper;

    /**
     * @var PredictHelper
     */
    protected $_predictHelper;

    /**
     * @var DPDRelaisHelper
     */
    protected $_relaisHelper;

    /**
     * @var
     */
    protected $result;

    /**
     * @var Order
     */
    protected $data;

    /**
     * @param Context $context
     * @param CollectionFactory $collectionFactory
     * @param Filter $filter
     * @param ShipmentsHelper $shipmentsHelper
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context           $context,
        CollectionFactory $collectionFactory,
        Filter            $filter,
        ShipmentsHelper   $shipmentsHelper,
        PageFactory       $resultPageFactory
    )
    {
        parent::__construct($context);
        $this->_collectionFactory = $collectionFactory;
        $this->_filter            = $filter;
        $this->_shipmentsHelper   = $shipmentsHelper;
        $this->result             = $resultPageFactory;
        $this->_setupHelper       = SetupHelper::getInstance();
        $this->_classicHelper     = DPDClassicHelper::getInstance();
        $this->_predictHelper     = DPDPredictHelper::getInstance();
        $this->_relaisHelper      = DPDRelaisHelper::getInstance();
    }

    /**
     * Récupère le code ISO du pays concerné et le convertit au format attendu par la Station DPD
     * @param string $idcountry
     * @return array|string|string[]
     */
    public static function getIsoCodebyIdCountry(string $idcountry)
    {
        $isops = [
            "DE", "AD", "AT", "BE", "BA", "BG", "HR", "DK", "ES", "EE", "FI", "FR", "GB", "GR", "GG", "HU",
            "IM", "IE", "IT", "JE", "LV", "LI", "LT", "LU", "NO", "NL", "PL", "PT", "CZ", "RO", "RS", "SK",
            "SI", "SE", "CH"
        ];
        $isoep = [
            "D", "AND", "A", "B", "BA", "BG", "CRO", "DK", "E", "EST", "SF", "F", "GB", "GR", "GG", "H",
            "IM", "IRL", "I", "JE", "LET", "LIE", "LIT", "L", "N", "NL", "PL", "P", "CZ", "RO", "RS", "SK",
            "SLO", "S", "CH"
        ];

        return in_array($idcountry, $isops) ?
            str_replace($isops, $isoep, $idcountry) :       // Si le code ISO est européen, on le convertit au format Station DPD
            str_replace($idcountry, "INT", $idcountry);     // Si le code ISO n'est pas européen, on le passe en "INT" (intercontinental)
    }

    /**
     * Reformat les caractères au format attendu pour la station
     * @param $str
     * @return array|string|string[]|null
     */
    private function stripAccents($str)
    {
        $str = preg_replace('/[\x{00C0}\x{00C1}\x{00C2}\x{00C3}\x{00C4}\x{00C5}]/u', 'A', $str);
        $str = preg_replace('/[\x{0105}\x{0104}\x{00E0}\x{00E1}\x{00E2}\x{00E3}\x{00E4}\x{00E5}]/u', 'a', $str);
        $str = preg_replace('/[\x{00C7}\x{0106}\x{0108}\x{010A}\x{010C}]/u', 'C', $str);
        $str = preg_replace('/[\x{00E7}\x{0107}\x{0109}\x{010B}\x{010D}}]/u', 'c', $str);
        $str = preg_replace('/[\x{010E}\x{0110}]/u', 'D', $str);
        $str = preg_replace('/[\x{010F}\x{0111}]/u', 'd', $str);
        $str = preg_replace('/[\x{00C8}\x{00C9}\x{00CA}\x{00CB}\x{0112}\x{0114}\x{0116}\x{0118}\x{011A}]/u', 'E', $str);
        $str = preg_replace('/[\x{00E8}\x{00E9}\x{00EA}\x{00EB}\x{0113}\x{0115}\x{0117}\x{0119}\x{011B}]/u', 'e', $str);
        $str = preg_replace('/[\x{00CC}\x{00CD}\x{00CE}\x{00CF}\x{0128}\x{012A}\x{012C}\x{012E}\x{0130}]/u', 'I', $str);
        $str = preg_replace('/[\x{00EC}\x{00ED}\x{00EE}\x{00EF}\x{0129}\x{012B}\x{012D}\x{012F}\x{0131}]/u', 'i', $str);
        $str = preg_replace('/[\x{0142}\x{0141}\x{013E}\x{013A}]/u', 'l', $str);
        $str = preg_replace('/[\x{00F1}\x{0148}]/u', 'n', $str);
        $str = preg_replace('/[\x{00D2}\x{00D3}\x{00D4}\x{00D5}\x{00D6}\x{00D8}]/u', 'O', $str);
        $str = preg_replace('/[\x{00F2}\x{00F3}\x{00F4}\x{00F5}\x{00F6}\x{00F8}]/u', 'o', $str);
        $str = preg_replace('/[\x{0159}\x{0155}]/u', 'r', $str);
        $str = preg_replace('/[\x{015B}\x{015A}\x{0161}]/u', 's', $str);
        $str = preg_replace('/[\x{00DF}]/u', 'ss', $str);
        $str = preg_replace('/[\x{0165}]/u', 't', $str);
        $str = preg_replace('/[\x{00D9}\x{00DA}\x{00DB}\x{00DC}\x{016E}\x{0170}\x{0172}]/u', 'U', $str);
        $str = preg_replace('/[\x{00F9}\x{00FA}\x{00FB}\x{00FC}\x{016F}\x{0171}\x{0173}]/u', 'u', $str);
        $str = preg_replace('/[\x{00FD}\x{00FF}]/u', 'y', $str);
        $str = preg_replace('/[\x{017C}\x{017A}\x{017B}\x{0179}\x{017E}]/u', 'z', $str);
        $str = preg_replace('/[\x{00C6}]/u', 'AE', $str);
        $str = preg_replace('/[\x{00E6}]/u', 'ae', $str);
        $str = preg_replace('/[\x{0152}]/u', 'OE', $str);
        $str = preg_replace('/[\x{0153}]/u', 'oe', $str);
        $str = preg_replace('/[\x{0022}\x{0025}\x{0026}\x{0027}\x{00B0}]/u', ' ', $str);
        return $str;
    }

    /**
     * Formater correctement le téléphone
     * Enlève les caractères spéciaux dans la chaine de caractères
     * @param string $tel_dest
     * @return array|string|string[]
     */
    private function correctTelephone(string $tel_dest)
    {
        return str_replace([' ', '.', '-', ',', ';', '/', '\\', '(', ')'], '', $tel_dest);
    }

    /**
     * Add a new field to the csv file
     * @param      $csvContent : the current csv content
     * @param      $fieldDelimiter : the delimiter character
     * @param      $fieldContent : the content to add
     * @param int  $size
     * @param bool $isNum
     * @return string : the concatenation of current content and content to add
     */
    private function _addFieldToCsv($csvContent, $fieldDelimiter, $fieldContent, int $size = 0, bool $isNum = false)
    {
        if (!$size) {
            return $csvContent . $fieldDelimiter . $fieldContent . $fieldDelimiter;
        }

        $newFieldContent = $fieldContent;
        if ($isNum) {
            for ($i = strlen($fieldContent); $i < $size; $i++) {
                $newFieldContent = '0' . $newFieldContent;
            }
        } else {
            for ($i = strlen($fieldContent); $i < $size; $i++) {
                $newFieldContent .= ' ';
            }
        }
        $newFieldContent = substr($newFieldContent, 0, $size);
        return $csvContent . $fieldDelimiter . $newFieldContent . $fieldDelimiter;
    }

    //Création du fichier dat pour la Station
    public function execute()
    {
        $collection = $this->_filter->getCollection($this->_collectionFactory->create()->addAttributeToSort('entity_id', 'DESC'));
        $lineBreak = "\r\n";
        $content   = '$VERSION=110' . $lineBreak;
        $delimiter = '';
        foreach ($collection->getItems() as $order) {
            $this->order = $order;
            $tabOrder    = $this->getAllValues();
            $content     = $this->_addFieldToCsv($content, $delimiter, $tabOrder["order_id"], 35);                                      // Ref Commande Magento
            $content     = $this->_addFieldToCsv($content, $delimiter, '', 2);                                                          // Filler
            $content     = $this->_addFieldToCsv($content, $delimiter, $tabOrder["s_weight"], 8, true);                                 // Poids
            $content     = $this->_addFieldToCsv($content, $delimiter, '', 15);

            if ($tabOrder["s_method"] !== 'dpdrelais_dpdrelais') {
                $content = $this->_addFieldToCsv($content, $delimiter, $tabOrder["s_lastname"] . ' ' . $tabOrder["s_firstname"], 35);   // Nom et prénom (Relais)
                $content = $this->_addFieldToCsv($content, $delimiter, '', 35);
            } else {
                $content = $this->_addFieldToCsv($content, $delimiter, $tabOrder["s_lastname"], 35);                                    // Nom
                $content = $this->_addFieldToCsv($content, $delimiter, $tabOrder["s_firstname"], 35);
            }
            $content = $this->_addFieldToCsv($content, $delimiter, $tabOrder["s_company"], 35);
            $content = $this->_addFieldToCsv($content, $delimiter, $tabOrder["s_addr3"], 35);
            $content = $this->_addFieldToCsv($content, $delimiter, $tabOrder["s_addr3"], 35);
            $content = $this->_addFieldToCsv($content, $delimiter, $tabOrder["s_addr4"], 35);
            $content = $this->_addFieldToCsv($content, $delimiter, $tabOrder["s_postcode"], 10);
            $content = $this->_addFieldToCsv($content, $delimiter, $tabOrder["s_city"], 35);
            $content = $this->_addFieldToCsv($content, $delimiter, '', 10);
            $content = $this->_addFieldToCsv($content, $delimiter, $tabOrder["s_street"], 35);
            $content = $this->_addFieldToCsv($content, $delimiter, '', 10);                                                             // Filler
            $content = $this->_addFieldToCsv($content, $delimiter, $tabOrder["s_country_id"], 3);                                       // Code pays
            $content = $this->_addFieldToCsv($content, $delimiter, $tabOrder["s_telephone"], 30);                                       // Téléphone
            $content = $this->_addFieldToCsv($content, $delimiter, '', 15);
            $content = $this->_addFieldToCsv($content, $delimiter, $tabOrder["e_raisonsociale"], 35);
            $content = $this->_addFieldToCsv($content, $delimiter, $tabOrder["e_addr2"], 35);                                           // Complément adresse 2
            $content = $this->_addFieldToCsv($content, $delimiter, '', 35);                                                             // Filler
            $content = $this->_addFieldToCsv($content, $delimiter, '', 35);                                                             // Filler
            $content = $this->_addFieldToCsv($content, $delimiter, '', 35);                                                             // Filler
            $content = $this->_addFieldToCsv($content, $delimiter, '', 35);
            $content = $this->_addFieldToCsv($content, $delimiter, $tabOrder["e_postcode"], 10);
            $content = $this->_addFieldToCsv($content, $delimiter, $tabOrder["e_city"], 35);
            $content = $this->_addFieldToCsv($content, $delimiter, '', 10);
            $content = $this->_addFieldToCsv($content, $delimiter, $tabOrder["e_street"], 35);                                          // Rue - adresse 1
            $content = $this->_addFieldToCsv($content, $delimiter, '', 10);                                                             // Filler
            $content = $this->_addFieldToCsv($content, $delimiter, 'F', 3);
            $content = $this->_addFieldToCsv($content, $delimiter, $tabOrder["e_telephone"], 30);
            $content = $this->_addFieldToCsv($content, $delimiter, '', 35);                                                             // Filler
            $content = $this->_addFieldToCsv($content, $delimiter, '', 35);                                                             // Filler
            $content = $this->_addFieldToCsv($content, $delimiter, '', 35);                                                             // Filler
            $content = $this->_addFieldToCsv($content, $delimiter, '', 35);                                                             // Filler
            $content = $this->_addFieldToCsv($content, $delimiter, date('d/m/Y'), 10);
            $content = $this->_addFieldToCsv($content, $delimiter, $tabOrder["e_contract"], 8, true);                                   // N° Compte chargeur
            $content = $this->_addFieldToCsv($content, $delimiter, $tabOrder["order_id"], 35);                                          // Code barres
            $content = $this->_addFieldToCsv($content, $delimiter, $tabOrder["order_id"], 35);                                          // N° Commande
            $content = $this->_addFieldToCsv($content, $delimiter, '', 29);
            if ($tabOrder["advalorem"] === 1) {
                $content = $this->_addFieldToCsv($content, $delimiter, substr($order->getGrandTotal(), 0, -2), 9, true);                // Assurance Ad Valorem
            } else {
                $content = $this->_addFieldToCsv($content, $delimiter, '', 9);                                                          // Pas d'assurance
            }
            $content = $this->_addFieldToCsv($content, $delimiter, '', 8);                                                              // Filler
            $content = $this->_addFieldToCsv($content, $delimiter, '', 35);                                                             // Ref client 2
            $content = $this->_addFieldToCsv($content, $delimiter, '', 1);                                                              // Filler
            $content = $this->_addFieldToCsv($content, $delimiter, '', 35);                                                             // Filler
            $content = $this->_addFieldToCsv($content, $delimiter, '', 10);
            $content = $this->_addFieldToCsv($content, $delimiter, $tabOrder["e_email"], 80);
            $content = $this->_addFieldToCsv($content, $delimiter, $tabOrder["e_telephone"], 35);
            $content = $this->_addFieldToCsv($content, $delimiter, $tabOrder["s_email"], 80);
            $prefixe = substr($tabOrder["s_telephone"], 0, 2);
            if (
                $tabOrder["s_method"] !== 'dpdclassic_dpdclassic' &&
                $tabOrder["s_country_id"] === 'F' &&
                ($prefixe == 06 || $prefixe == 07)
            ) {
                $content = $this->_addFieldToCsv($content, $delimiter, $tabOrder["s_telephone"], 35);
            } else if ($tabOrder["s_method"] === 'dpdclassic_dpdclassic') {
                $content = $this->_addFieldToCsv($content, $delimiter, $tabOrder["s_telephone"], 35);
            } else {
                $content = $this->_addFieldToCsv($content, $delimiter, '', 35);
            }

            $content = $this->_addFieldToCsv($content, $delimiter, '', 96);

            if ($tabOrder["s_method"] === "dpdrelais_dpdrelais") {
                $content = $this->_addFieldToCsv($content, $delimiter, $tabOrder["relay_id"], 8);
            } else {
                $content = $this->_addFieldToCsv($content, $delimiter, '', 8);
            }

            $content = $this->_addFieldToCsv($content, $delimiter, '', 118);

            if ($tabOrder["s_method"] === "dpdpredict_dpdpredict" && $tabOrder["s_telephone"] && $tabOrder["s_country_id"] === 'F') {
                $content = $this->_addFieldToCsv($content, $delimiter, '+', 1);                                                         // Flag Predict
            } else {
                $content = $this->_addFieldToCsv($content, $delimiter, '', 1);
            }

            $content = $this->_addFieldToCsv($content, $delimiter, $tabOrder["s_lastname"], 35);                                        // Nom du contact
            $content = $this->_addFieldToCsv($content, $delimiter, '', 230);

            if ($tabOrder["retour"] != 0) {
                $content = $this->_addFieldToCsv($content, $delimiter, $tabOrder["retour"], 1);                                         // DPD Retour
            } else {
                $content = $this->_addFieldToCsv($content, $delimiter, '', 1);                                                          // Pas de retour
            }

            $content = $this->_addFieldToCsv($content, $delimiter, '', 411);
            $content .= $lineBreak;
        }

        $content = utf8_decode($content);
        header('Content-type: application/dat');
        header('Content-Disposition: attachment; filename="DPDFRANCE_' . date('dmY-His') . '.dat"');
        echo $content . "\r\n";
        exit;
    }

    /**
     * @return array
     */
    public function getAllValues(): array
    {
        $tabValues                    = [];
        /** @var Order\Address $s_address */
        $s_address                    = $this->order->getShippingAddress();
        $tabValues["order_id"]        = $this->order->getRealOrderId();
        $tabValues["s_weight"]        = floor($this->order->getWeight() * 100);
        $tabValues["s_method"]        = $this->order->getShippingMethod();
        $tabValues["s_firstname"]     = $this->stripAccents($s_address->getFirstname());
        $tabValues["s_lastname"]      = $this->stripAccents($s_address->getLastname());
        $tabValues["s_company"]       = $this->stripAccents($s_address->getCompany());
        $tabValues["s_addr3"]         = $this->stripAccents($s_address->getStreetLine(3));
        $tabValues["s_addr4"]         = $this->stripAccents($s_address->getStreetLine(4));
        $tabValues["s_postcode"]      = $s_address->getPostcode();
        $tabValues["s_city"]          = $this->stripAccents($s_address->getCity());
        $tabValues["s_street"]        = $this->stripAccents($s_address->getStreet()[0]);
        $tabValues["s_country_id"]    = self::getIsoCodebyIdCountry($s_address->getCountryId());
        $tabValues["s_telephone"]     = $this->correctTelephone($s_address->getTelephone());
        $tabValues["e_raisonsociale"] = $this->stripAccents($this->_setupHelper->getName());
        $tabValues["e_addr2"]         = $this->stripAccents($this->_setupHelper->getAddress2());
        $tabValues["e_postcode"]      = $this->_setupHelper->getZipCode();
        $tabValues["e_city"]          = $this->stripAccents($this->_setupHelper->getCity());
        $tabValues["e_street"]        = $this->stripAccents($this->_setupHelper->getAddress1());
        $tabValues["e_country_id"]    = "F";
        $tabValues["e_telephone"]     = $this->correctTelephone($this->_setupHelper->getPhoneNumber());
        $tabValues["e_date_expe"]     = date("d/m/Y");
        $tabValues["e_contract"]      = "";

        switch ($tabValues["s_method"]) {
            case "dpdclassic_dpdclassic":
                $tabValues["e_contract"] = $this->_classicHelper->getContract();
                break;
            case "dpdpredict_dpdpredict":
                $tabValues["e_contract"] = $this->_predictHelper->getContract();
                break;
            case "dpdrelais_dpdrelais":
                $tabValues["e_contract"] = $this->_relaisHelper->getContract();
                break;
        }

        $tabValues["s_grand_total"] = $this->order->getGrandTotal();
        $tabValues["e_email"]       = $this->_setupHelper->getEmail();
        $tabValues["e_telephone"]   = $this->correctTelephone($this->_setupHelper->getPhoneNumber());
        $tabValues["s_email"]       = $s_address->getEmail();
        $company                    = explode(" ", $tabValues["s_company"]);
        $tabValues["relay_id"]      = $company[count($company) - 1];
        $tabValues["retour"]        = $this->_shipmentsHelper->getRetour($this->order->getRealOrderId());
        $tabValues["advalorem"]     = (int)$this->_shipmentsHelper->getAdvalorem($this->order->getRealOrderId());

        return $tabValues;
    }
}
