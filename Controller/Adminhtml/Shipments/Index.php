<?php

namespace DPDFrance\ShippingM2\Controller\Adminhtml\Shipments;

use DPDFrance\ShippingM2\Helper\Adminhtml\ShipmentsHelper;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;

/**
 * * Cette classe est un controller et sera utilisé pour la gestion des expéditions
 */
class Index extends Action
{
    /**
     * @param Context $context
     */
    public function __construct(Context $context)
    {
        parent::__construct($context);
    }

    public function execute()
    {
        ShipmentsHelper::getInstance()->deleteFromDPDOptionCompleteOrder();
        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }
}
