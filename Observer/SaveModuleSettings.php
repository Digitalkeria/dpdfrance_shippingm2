<?php

namespace DPDFrance\ShippingM2\Observer;

use DPDFrance\ShippingM2\Helper\Adminhtml\ShipmentsHelper;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;

/**
 * Cette classe a ete cree pour la verification des champs dans l'admin
 */
class SaveModuleSettings implements ObserverInterface
{
    private $request;
    /**
     * @var ShipmentsHelper
     */
    private $shipmentsHelper;

    public function __construct(
        RequestInterface $request
    )
    {
        $this->request         = $request;
        $this->shipmentsHelper = ShipmentsHelper::getInstance();
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer): void
    {
        $dpdSettingsDatas = $this->request->getParam('groups');
        $adValoremValue   = $dpdSettingsDatas['shipping_gestion']['fields']['assurance']['value'];
        $adRetourValue    = $dpdSettingsDatas['shipping_gestion']['fields']['retour']['value'];
        $this->shipmentsHelper->updateInDPDOptionAdvaloremSettings($adValoremValue);
        $this->shipmentsHelper->updateInDPDOptionRetourSettings($adRetourValue);
    }

}
