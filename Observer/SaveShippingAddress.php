<?php

namespace DPDFrance\ShippingM2\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\Order;

/**
 * Cette classe sera utilisee pour le checkout lorsque le bouton "Next" ou "PlaceOrder" aura un clic dessus
 */
class SaveShippingAddress implements ObserverInterface
{
    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        /** @var Order $order */
        $order = $observer->getEvent()->getOrder();
    }
}
