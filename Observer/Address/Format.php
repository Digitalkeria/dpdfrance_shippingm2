<?php
declare(strict_types=1);

namespace DPDFrance\ShippingM2\Observer\Address;

use DPDFrance\ShippingM2\Model\Service\Controller;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\Order\Address;

class Format implements ObserverInterface
{
    /**
     * @var Controller
     */
    private $serviceController;

    public function __construct(
        Controller $serviceController
    ) {
        $this->serviceController    = $serviceController;
    }

    public function execute(Observer $observer)
    {
        if ($this->serviceController->isOrderView()) {
            /** @var $address Address */
            $address = $observer->getData('address');
            $address->setRegion('');
            $address->setRegionCode('');
            $address->setRegionId(0);
        }
    }
}
